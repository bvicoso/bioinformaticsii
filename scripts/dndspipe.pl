#!/usr/bin/perl
my $a = $ARGV[0];
my $b = $ARGV[1];
my $out = $ARGV[2];

############run Blat in both directions, sort results
system "mkdir $out";
system "module load blat/20170224";
system "blat -t=dnax -q=dnax -minScore=100 $a $b /dev/stdout | sort -k 10 > $out/ab.blat";
system "blat -t=dnax -q=dnax -minScore=100 $b $a /dev/stdout | sort -k 10 > $out/ba.blat";


############Keep only best hits for species b
open (INPUT, "$out/ab.blat"); 
open (RESULTS, ">$out/ab.blat.besthit");

print RESULTS "1match\tmismatch\trep\tNs\tQgapcount\tQgapbases\tTgapcount\tTgapbases\tstrand\tQname\tQsize\tQstart\tQend\tTname\tTsize\tTstart\tTend\tblockcount\tblockSizes\tqStarts\ttStarts\n";

$name0="";
$score0=0;
$line0="";

while ($line = <INPUT>) {
($match, $mismatch, $rep, $Ns, $Qgapcount, $Qgapbases, $Tgapcount, $Tgapbases, $strand, $Qname, $Qsize, $Qstart, $Qend, $Tname, $Tsize, $Tstart, $Tend, $blockcount, $blockSizes, $qStarts, $tStarts)=split(/\t/, $line);
	
	if ($Qname eq $name0)
		{
		if ($match > $score0)
			{
			$name0=$Qname;
			$score0=$match;
			$line0=$line;
			}
		else
			{
			}

		}
	else
		{
		print RESULTS $line0;
		$name0=$Qname;
                $score0=$match;
                $line0=$line;

		}
		
	
	}

print RESULTS $line0;

system "perl -pi -e 's/^[^0-9].*//gi' $out/ab.blat.besthit";
system "perl -pi -e 's/1match/match/gi' $out/ab.blat.besthit";
system "perl -pi -e 's/^\n//gi' $out/ab.blat.besthit";

############Keep only best hits for species a

open (INPUT, "$out/ba.blat"); 
open (RESULTS, ">$out/ba.blat.besthit");

print RESULTS "1match\tmismatch\trep\tNs\tQgapcount\tQgapbases\tTgapcount\tTgapbases\tstrand\tQname\tQsize\tQstart\tQend\tTname\tTsize\tTstart\tTend\tblockcount\tblockSizes\tqStarts\ttStarts\n";

$name0="";
$score0=0;
$line0="";

while ($line = <INPUT>) {
($match, $mismatch, $rep, $Ns, $Qgapcount, $Qgapbases, $Tgapcount, $Tgapbases, $strand, $Qname, $Qsize, $Qstart, $Qend, $Tname, $Tsize, $Tstart, $Tend, $blockcount, $blockSizes, $qStarts, $tStarts)=split(/\t/, $line);
	
	if ($Qname eq $name0)
		{
		if ($match > $score0)
			{
			$name0=$Qname;
			$score0=$match;
			$line0=$line;
			}
		else
			{
			}

		}
	else
		{
		print RESULTS $line0;
		$name0=$Qname;
                $score0=$match;
                $line0=$line;

		}
		
	
	}

print RESULTS $line0;

system "perl -pi -e 's/^[^0-9].*//gi' $out/ba.blat.besthit";
system "perl -pi -e 's/1match/match/gi' $out/ba.blat.besthit";
system "perl -pi -e 's/^\n//gi' $out/ba.blat.besthit";



#####Create a list of reciprocal best hits

open (AB, "$out/ab.blat.besthit") or die "where is blat best hit file for first species?"; 
open (BA, "$out/ba.blat.besthit") or die "where is blat best hit file for second species?"; 
open (RESULTS, ">$out/ab.reciprocalbesthits");

#make hash with SpA genes and their best hit, as well as list of SpA genes
while ($line=<AB>) 
	{
	next if $. < 2; # Skip first line
	($match, $mismatch, $rep, $Ns, $Qgapcount, $Qgapbases, $Tgapcount, $Tgapbases, $strand, $Qname, $Qsize, $Qstart, $Qend, $Tname, $Tsize, $Tstart, $Tend, $blockcount, $blockSizes, $qStarts, $tStarts)=split(/\t/, $line);
	chomp $line;
	push(@list, $Qname);
	$best1{$Qname} = $Tname;

        }

#make hash with SpB genes and their best hit
while ($line=<BA>) 
	{
	next if $. < 2; # Skip first line
	($match, $mismatch, $rep, $Ns, $Qgapcount, $Qgapbases, $Tgapcount, $Tgapbases, $strand, $Qname, $Qsize, $Qstart, $Qend, $Tname, $Tsize, $Tstart, $Tend, $blockcount, $blockSizes, $qStarts, $tStarts)=split(/\t/, $line);
	chomp $line;
	$best2{$Qname} = $Tname;

        }

#Compare gene_spa and best2(best1(gene_spa)) 
foreach (@list)
        {
	if ( $best2{$best1{$_}} eq $_  )
		{
		print RESULTS "$best1{$_} $_\n";
		}
	else 
		{
		}
	}
close (AB);

###########Make individual fasta files for each pair of reciprocal best hits

open (AB, "$out/ab.reciprocalbesthits") or die "where are the reciprocal best hits?"; 
system "mkdir $out/fasta";

while ($line = <AB>) 
	{
	($aname, $bname)=split(/\s/, $line);
	open (TEMP1, ">$out/temp1");
	print TEMP1 "$aname\n";
	system "/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq $a $out/temp1 >> $out/fasta/$aname.fasta";
	open (TEMP2, ">$out/temp2");
	print TEMP2 "$bname\n";
	system "/nfs/scistore03/vicosgrp/bvicoso/seqtk/seqtk subseq $b $out/temp2 >> $out/fasta/$aname.fasta";
	close (TEMP1);
	close (TEMP2);

	}



##########make alignments, axt file and run KaKs_calculator

open (AB, "$out/ab.reciprocalbesthits") or die "where are the reciprocal best hits?"; 
system "module load gblocks/0.91b";

system "mkdir $out/translatorX";
system "mkdir $out/KaKs";

while ($line = <AB>) 
	{
	($aname, $bname)=split(/\s/, $line);
	system "perl -pi -e \'s/ \.\*//gi\' $out/fasta/$aname.fasta";
	system "perl /nfs/scistore03/vicosgrp/bvicoso/translatorx_vLocal.pl -i $out/fasta/$aname.fasta -o $out/translatorX/$aname.tx -t T -g \"-s\"";
	system "perl /nfs/scistore03/vicosgrp/bvicoso/parseFastaIntoAXT.pl $out/translatorX/$aname.tx.nt_cleanali.fasta";
	system " /nfs/scistore03/vicosgrp/bvicoso/KaKs_Calculator2.0/bin/Linux/KaKs_Calculator -i $out/translatorX/$aname.tx.nt_cleanali.fasta.axt -o $out/KaKs/$aname.kaks -m NG -m YN";

	}


