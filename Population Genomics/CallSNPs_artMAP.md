# SNP analysis following artMAP pipeline

artMAP is a "user‐friendly tool for mapping ethyl methanesulfonate‐induced mutations in Arabidopsis" [(link)](https://onlinelibrary.wiley.com/doi/full/10.1002/pld3.146).

We will follow the same pipeline for calling SNPs, but doing it step by step to allow for parameter changes:

![artmap](/images/ArtmapPipeline.jpg)


The reads have already been obtained/converted, and we start with fastq files.

The Genome was obtained from: ??


# Read trimming with Trimgalore

"The data are first processed by Trimgalore, which remove sequencing adapters as well as bad quality sequences from the reads."

```
#load trimgalore, cutadapt and fastqc
module load trimgalore/0.5.0 
module load cutadapt 
module load fastqc/0.11.7  
#Run trimgalore
srun trim_galore --fastqc --paired -q 20 --length 35 reads_1.fastq reads_2.fastq
```

[Main change relative to default: keep only reads longer than 35 post-trimming (20 seems short for SNP calling, as it may lead to misalignments):
--fastqc: Run FastQC in the default mode on the FastQ file once trimming is complete.
--paired: This option performs length trimming of quality/adapter/RRBS trimmed reads for paired-end files. To pass the validation test, both sequences of a sequence pair are required to have a certain minimum length which is governed by the option --length (see above). If only one read passes this length threshold the other read can be rescued (see option --retain_unpaired). Using this option lets you discard too short read pairs without disturbing the sequence-by-sequence order of FastQ files which is required by many aligners.
--length <INT>: Discard reads that became shorter than length INT because of either quality or adapter trimming. A value of '0' effectively disables this behaviour. Default: 20 bp.
-q/--quality <INT>: Trim low-quality ends from reads in addition to adapter removal. For RRBS samples, quality trimming will be performed first, and adapter trimming is carried in a second round. Other files are quality and adapter trimmed in a single pass. The algorithm is the same as the one used by BWA (Subtract INT from all qualities; compute partial sums from all indices to the end of the sequence; cut sequence at the index at which the sum is minimal). Default Phred score: 20.]

# Alignment with BWA (aln/mem)

"Next, high-quality sequencing reads from the mutant and parent samples are aligned to the Arabidopsis reference genome by BWA. The major advantage of BWA is the ability to align both short and long reads. artMAP include “BWA aln” as well as “BWA mem” for aligning shorter and longer reads, respectively, and artMAP can choose the appropriate aligner based on the length of the input reads."
From the BWA page: For 70bp or longer Illumina, 454, Ion Torrent and Sanger reads, assembly contigs and BAC sequences, BWA-MEM is usually the preferred algorithm. For short sequences, BWA-backtrack may be better. BWA-SW may have better sensitivity when alignment gaps are frequent.

```
#Download the genome
wget ftp://ftp.ensemblgenomes.org/pub/plants/release-43/fasta/arabidopsis_thaliana/dna/Arabidopsis_thaliana.TAIR10.dna.toplevel.fa.gz
# Unpack the genome
gunzip -c Arabidopsis_thaliana.TAIR10.dna.toplevel.fa.gz > Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
```
In a SLURM script, start the read alignment using BWA:

```
#Load BWA
module load bwa/0.7.17
#Index the genome. This step only needs to be done once
bwa index Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
#Map paired end reads (do for mutant and wild type)
bwa mem Arabidopsis_thaliana.TAIR10.dna.toplevel.fa read1.fq read2.fq > aln-pe.sam
```
(replace read1.fq read2.fq with the read names; you should have one line per set of reads, and a different output name for each_


# Post-processing with SAMtools

"SAMtools is used to convert SAM to BAM files, 

```
#load any module here
module load samtools/1.8 
#run commands here
#SAM to BAM
samtools view -S -b sample.sam > sample.bam
```


* OPTION 1: No PCR removal: "BAM files  are then further sorted according to chromosome number and the genomic location of aligned reads"

```
#load any module here
module load samtools/1.8 
#Because BWA can sometimes leave unusual FLAG information on SAM records, it is helpful when working with many tools to first clean up read pairing information and flags:
srun samtools fixmate -O bam control1.bam control1_fixmate.bam
#sort such that the alignments occur in “genome order” [ordered positionally based upon their alignment coordinates on each chromosome.]
srun samtools sort -O bam control1_fixmate.bam -o control1_sorted.bam 
```


* OPTION 2: with PCR removal:"artMAP provides an additional option to control the removal of PCR duplicates from the control and mutant BAM files. This step is turned on by default but can be disabled if required."

```
#load any module here
module load samtools/1.8 
# sort BAM file by name (-n option) 
srun samtools sort -n control1.bam -o control1_namesort.bam 
# Add ms and MC tags for markdup to use later
srun samtools fixmate -m control1_namesort.bam control1_fixmate.bam
# Markdup needs position order
srun samtools sort control1_fixmate.bam -o control1_positionsort.bam
# Finally remove (-r option) duplicates
srun samtools markdup -r control1_positionsort.bam control1_markdup.bam
```


"Control and mutant BAM files are indexed to prepare for SNP calling with SAMtools."

```
#load any module here
module load samtools/1.8 
#run commands here
samtools index control1_sorted.bam [or use control1_markdup.bam]
```

# SNP identification: SAMtools/BCFtools

"SNPs are identified from both the control and mutant BAM files generated in the previous step."
4.1. index reference genome with SAMtools

module load samtools/1.8 

samtools faidx ref.fasta
4.2. Use bcftools mpileup to Generate VCF or BCF containing genotype likelihoods for one or multiple alignment (BAM or CRAM) files.

module load bcftools/1.8

bcftools mpileup -Ou -f ref.fa control1_sorted.bam control2_sorted.bam control3_sorted.bam | bcftools call -vmO z -o control.vcf.gz



#Explanation of the options:

#mpileup options: 

# -O, --output-type TYPE 'u' uncompressed BCF;

# -f, --fasta-ref FILE    faidx indexed reference sequence file

#call options:

# -v, --variants-only             output variant sites only

# -m, --multiallelic-caller       alternative model for multiallelic and rare-variant calling (conflicts with -c)

# -O, --output-type <b|u|z|v>     'z' compressed VCF

# -o, --output <file>             write output to a file [standard output]



NB: SAMtools has a suggested workflow to call SNPs here, which includes improvement of the alignments with GATK. SInce this is not mentioned in artMAP, we are not doing it. But it may be worth trying it to see if it changes the results. GATK is on the cluster (module load gatk/4.0.3.0), so it should not be too hard to implement the protocol:

http://www.htslib.org/workflow/#mapping_to_variant

5. SNP annotation (SNPeff)


"These SNPs are then annotated using SnpEff (Cingolani et al., 2012) to describe their impact on gene structure and amino acid changes. Nonsense mutations producing a stop codon are considered high impact SNPs."


module load SnpEff/4.3t

snpEff.jar databases | grep "Arabidopsis"

curl -O -L http://downloads.sourceforge.net/project/snpeff/databases/v4_3/snpEff_v4_3_ENSEMBL_BFMPP_32_24.zip
unzip snpEff_v4_3_ENSEMBL_BFMPP_32_24.zip

mkdir -p snpEff/data/Arabidopsis_thaliana/

mv home/pcingola/snpEff/data/Arabidopsis_thaliana/ snpEff/data/Arabidopsis_thaliana/

In a script, you will want to run:

snpEff.jar ann snpEff/data/Arabidopsis_thaliana input.vcf > input.snpeff.vcf

6. SNP filtration


"As EMS induces G to A or C to T transitions in DNA, artMAP only retains these SNPs."

" artMAP applies two filter criteria to remove background polymorphisms that may occur as technical errors. 

- First, artMAP removes SNPs with a frequency lower than 30%. 

- Second, it applies a depth filter to retain SNPs with a sequencing depth between 10-100X. 

Since these filters can greatly affect the final outcome of the analysis, they can be changed in additional settings."


cat control.snpeff.vcf | SnpSift.jar filter " ( REF = 'C' ) & ( ALT = 'T' ) & ( AF < 0.3 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'MODERATE') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > control_C_T_moderate.txt

cat control.snpeff.vcf | SnpSift.jar filter " ( REF = 'C' ) & ( ALT = 'T' ) & ( AF < 0.3 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'HIGH') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > control_C_T_high.txt


cat control.snpeff.vcf | SnpSift.jar filter " ( REF = 'G' ) & ( ALT = 'A' ) & ( AF < 0.3 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'MODERATE') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > control_G_A_moderate.txt

cat control.snpeff.vcf | SnpSift.jar filter " ( REF = 'G' ) & ( ALT = 'A' ) & ( AF < 0.3 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'HIGH') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > control_G_A_high.txt


cat experiment.snpeff.vcf | SnpSift.jar filter " ( REF = 'C' ) & ( ALT = 'T' ) & ( AF > 0.6 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'MODERATE') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > experiment_C_T_moderate.txt

cat experiment.snpeff.vcf | SnpSift.jar filter " ( REF = 'C' ) & ( ALT = 'T' ) & ( AF > 0.6 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'HIGH') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > experiment_C_T_high.txt


cat experiment.snpeff.vcf | SnpSift.jar filter " ( REF = 'G' ) & ( ALT = 'A' ) & ( AF > 0.6 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'MODERATE') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > experiment_G_A_moderate.txt

cat experiment.snpeff.vcf | SnpSift.jar filter " ( REF = 'G' ) & ( ALT = 'A' ) & ( AF > 0.6 ) & ( DP >= 10 ) & (DP <= 100) & (ANN[*].IMPACT = 'HIGH') "  | SnpSift.jar extractFields - CHROM POS REF ALT AF ANN[*].EFFECT  ANN[*].IMPACT > experiment_G_A_high.txt


7. SNP subtraction

"Next, to identify the region of the genome associated with the phenotype of interest, artMAP compares the SNPs present in the control and mutant sample and extracts SNPs exclusive to the mutant."



8. Visualization

"artMAP displays the final results as a graph, where the frequency and position of each SNP are plotted along each Arabidopsis chromosome."

