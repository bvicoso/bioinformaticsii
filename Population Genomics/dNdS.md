# Estimate dN and dS using transcriptomes from two species

This pipeline can be used to find best-reciprocal-hits from the coding sequences of two species, and estimate their dN and dS. Make sure you use CODING SEQUENCES, and not just transcripts that may not be in frame. A very simple program to extract coding sequences can be found [here](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Genome%20%252F%20Transcriptome%20Assembly/GetORF.md), but more sophisticated ones (e.g. Genewise) exist.

## General steps

The pipeline goes through the following steps
* blat species 1 versus species 2 and vice-versa
* find reciprocal best hits (stored in outputfolder/ab.reciprocal)
* make fasta files for each pair of reciprocal best hits (stored in outputfolder/fasta)
* align with translatorX (a codon-aware aligner), which includes a step to remove unreliable alignments with gblocks (stored in outputfolder/translatorX)
* switch alignment from fasta to AXT format
* estimate dN and dS using KaKs_Calculator (the final dNdS files are stored in outputfolder/KaKs). 

We currently estimate dN and dS using two different substitution models: Nei-Gojobori (NG), and Yang-Nielsen 2000 (YN). These yield similar estimates as long as dS and dN are small. When they are larger, YN tends to yield even larger values.

## Script

This script relies on a few locally installed programs:
* /nfs/scistore03/vicosgrp/bvicoso/translatorx_vLocal.pl
* /nfs/scistore03/vicosgrp/bvicoso/parseFastaIntoAXT.pl
* /nfs/scistore03/vicosgrp/bvicoso/KaKs_Calculator2.0/bin/Linux/KaKs_Calculator

I've given executing powers to all users for those files, so it should work. But if not, re-install these locally and change the path in the perl script.

The script can be found [here](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/scripts/dndspipe.pl). 

## Usage: 

The pipeline takes as input the two CDS files, as well as the name of the output folder to be used/created. Make sure to load blat, gblocks and muscle.

```
module load blat/20170224
module load gblocks/0.91b
module load muscle

srun perl dndspipe.pl CDS_species1.fasta CDS_species2.fasta outputfolder
```

## Parsing results

```
cat outputfolder/KaKs/* | cut -f 1,2,3,4,5,6,7 | grep 'NG' > dNdStable_NG.txt
cat outputfolder/KaKs/* | cut -f 1,2,3,4,5,6,7 | grep 'YN' > dNdStable_YN.txt
```

The columns are:
* Sequence names, separated by "-"	
* Method ("NG" or "YN")
* dN
* dS
* dN/dS	
* P-Value(Fisher) of comparison between dN/dS and 1
* Length of the alignment

