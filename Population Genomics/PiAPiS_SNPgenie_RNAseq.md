# Estimate PiN, PiS, dN and dS with RNA-seq and a transcriptome with SNPgenie

SNPgenie typically uses a reference genome and a GTF annotation file to estimate PiA and PiS. Here, the pipeline is adapted for a transcriptome and RNA-seq data. We first make a "pseudo-genome" from the transcripts, and the corresponding GTF file. Then we run SNP genie.

The input is:
* a set of CODING SEQUENCES in a fasta file. If you assembled a transcriptome de novo, make sure to first extract coding sequences using ORFfinder or the Evigene pipeline;
* RNA-seq reads

Programs/scripts required:
* SNPgenie
* MakeGTFcombined.pl
* AdjustVCF.pl

These are available [here](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Population%20Genomics/SNPGenie_trans_300420.gz) or on the cluster at: /nfs/scistore03/vicosgrp/Bioinformatics_2020/BV_bioinfo2/SNPGenie_trans_300420.gz

# make single fasta sequence

```
cat Asinica500.fa | grep -v '>' > Asinica_singleSeq.fa
sed '1i >fasta1' Asinica_singleSeq.fa > Asinica_singleSeq_head.fa
```

# Make the corresponding GTF file

```
perl MakeGTFcombined.pl Asinica500.fa

```

## Option 1 for running SNPgenie:
If you do not have a VCF file already, this is be sufficient: just align the reads to the combined fasta file to call genetic variants (SNPs) into a new VCF file (see how we do that [here](/Population%20Genomics/Admixture.md) or [here](/Population%20Genomics/CallSNPs_artMAP.md)), and use the “Asinica500.fa.fake.gtf” file to run SNPgenie as described in their manual.

## Option 2: if you aligned your reads to the transcriptome directly
In our case, we produced the VCF files following the steps shown in the [ancestry pipeline](/Population%20Genomics/Admixture.md)

We now select the samples that we want, filter each of them, and run the "AdjustVCF.pl" tool to fix the coordinates to those of the pseudo-genome.

```
#load any modules here
module load vcftools
#VCFtools (0.1.15), April 1st 2020
module load bcftools
#Version: 1.8 (using htslib 1.8)

#Make_Fake_VCF.sh
srun bcftools view -s sin_39897.bam AllSamples.vcf.gz |  vcftools --vcf - --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 200 --minDP 10 --maxDP 200 --recode --stdout > sin_39897_filtered.vcf
srun perl AdjustVCF.pl Asinica500.fa sin_39897_filtered.vcf
```

(This will create the file sin_39897_filtered.vcf.fake ; do this for each sample to be analysed)


We need to set up the correct file structure, since SNPgenie always creates the same output directory! (WHY??)

```
mkdir SNPgenieHeads
mkdir SNPgenieHeads/sin_39897 SNPgenieHeads/sin_39898 SNPgenieHeads/urmsex_61775 SNPgenieHeads/urmsex_61779 
mv sin_39897_filtered.vcf.fake SNPgenieHeads/sin_39897/fake.vcf
mv sin_39898_filtered.vcf.fake SNPgenieHeads/sin_39898/fake.vcf
mv urmsex_61775_filtered.vcf.fake SNPgenieHeads/urmsex_61775/fake.vcf
mv urmsex_61779_filtered.vcf.fake SNPgenieHeads/urmsex_61779/fake.vcf
```

Then run SNPgenie in each folder within SNPgenieHead (SNPgenie.sh)

```
srun /nfs/scistore03/vicosgrp/bvicoso/scripts/SNPGenie-master/snpgenie.pl --minfreq=0.01 --vcfformat=4 --snpreport=fake.vcf --fastafile=/nfs/scistore03/vicosgrp/bvicoso/Artemia_Popgen_Oct2019/2-Alignments/GetPi/Asinica_singleSeq_head.fa --gtffile=/nfs/scistore03/vicosgrp/bvicoso/Artemia_Popgen_Oct2019/2-Alignments/GetPi/Asinica500.fa.fake.gtf
```

## Processing of results

Let's now make a large file that contains only the PiA and PiS values for each sample. We first merge the results tables from the different samples:

```
#used paste command to column-bind all results files
#make sure that you are in the SNPgenieHeads/ folder
cd /path/to/SNPgenieHeads/
#combine all the files into a giant table
paste */SNPGenie_Results/product_results.txt
```

Then select only columns with ‘pi’ in their name. We first transpose the table so columns are now lines, then use grep to select lines that match the term "Pi", then transpose the table again - not the most straightforward way but I could not find a better one.

```
#transpose command from: http://archive.sysbio.harvard.edu/CSB/resources/computational/scriptome/UNIX/Tools/Change.html
cat Heads_SNPgenie.txt | perl -e ' $unequal=0; $_=<>; s/\r?\n//; @out_rows = split /\t/, $_; $num_out_rows = $#out_rows+1; while(<>) { s/\r?\n//; @F = split /\t/, $_; foreach $i (0 .. $#F) { $out_rows[$i] .= "\t$F[$i]"; } if ($num_out_rows != $#F+1) { $unequal=1; } } END { foreach $row (@out_rows) { print "$row\n" } warn "\nWARNING! Rows in input had different numbers of columns\n" if $unequal; warn "\nTransposed table: result has $. columns and $num_out_rows rows\n\n" } ' | grep 'pi'  | perl -e ' $unequal=0; $_=<>; s/\r?\n//; @out_rows = split /\t/, $_; $num_out_rows = $#out_rows+1; while(<>) { s/\r?\n//; @F = split /\t/, $_; foreach $i (0 .. $#F) { $out_rows[$i] .= "\t$F[$i]"; } if ($num_out_rows != $#F+1) { $unequal=1; } } END { foreach $row (@out_rows) { print "$row\n" } warn "\nWARNING! Rows in input had different numbers of columns\n" if $unequal; warn "\nTransposed table: result has $. columns and $num_out_rows rows\n\n" } ' > Heads_SNPgeniePiaPis.txt
```
