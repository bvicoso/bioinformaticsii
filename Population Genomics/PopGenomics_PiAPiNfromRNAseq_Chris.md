# Estimate PiA and PiS from RNA-seq data with PopPhyl

Chris provided her pipeline for estimating PiA and PiS from RNA-seq data and a reference transcriptome using the PopPhyl package. 

It is available here:

```
https://seafile.ist.ac.at/f/6159f7c1d7e846aa996f/?dl=1
```