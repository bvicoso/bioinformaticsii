# Admixture analysis with SAMtools, BCFtools, VCFtools, Plink and Admixture

"[ADMIXTURE is] a high-performance tool for estimating individual ancestries and population allele frequencies from SNP (single nucleotide polymorphism) data. " (https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-246)

Since ADMIXTURE is not installed centrally, you can download it to your folder, or use the version located here: /nfs/scistore03/vicosgrp/Bioinformatics_2020/BV_bioinfo2/admixture_linux-1.3.0. 

Let’s use the steps described here on how to run a full ancestry analysis: https://speciationgenomics.github.io/variant_calling/

As an example, we are mapping RNA-seq reads from different populations of Artemia to an Artemia transcriptome, but you could use genomic reads mapped to a genome.

# 1. Map reads to transcriptome using bwa

First, index the transcriptome (or genome), in our case a fasta file called "Asinica500.fa".

```
module load bwa
srun bwa index Asinica500.fa
```

Then for each reads file, map the reads to the indexed transcriptome:

```
module load bwa
srun bwa mem -M -t 30 Asinica500.fa sample_1.fastq.gz sample_2.fastq.gz | samtools view -b | samtools sort -T individual > sample_sort.bam
```
(replace "sample" by your sample name; you should have one bwa map command per sample)

# 2. SNP calling using SAMtools, BCFtools and VCFtools

We detect genetic variants (SNPs) in our samples using BCFtools, and filter them using VCFtools (for mininmum and maximum read coverage and mapping quality) as well as BCFtools (to remove multi-allelic sites, which can be present when pooled individuals were used to produce the sequencing libraries, and which confuse ADMIXTURE).

```
module load samtools
module load bcftools
module load vcftools

#first index the transcriptome
srun samtools faidx Asinica500.fa

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f Asinica500.fa sin_39897.bam
sin_39898.bam kaz_41809.bam kaz_41810.bam urmsex_61775.bam urmsex_61779.bam aib_40773.bam aib_40774.bam ata_45191.bam ata_45192.bam urmas_45056.bam urmas_45057.bam | bcftools call -v -f GQ,GP -mO z -o head.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout >  head_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_filtered.vcf > head_filtered2.vcf
```

# 3. Plink filtering and formatting

Plink is a multi-use genomics tool. Here, we use it for two reasons: 1. to remove "linked SNPs" (e.g. SNPs that are present in the same individuals, suggesting that they are closely related on the chromosome, and do not provide independent information about the origin of the individual) 2. to produce the BED files required by ancestry.


```
/nfs/scistore03/vicosgrp/bvicoso/Artemia_Popgen_Oct2019/3-Plink
 cp ../2-Alignments/head_filtered2.vcf .

# prune and create bed files 
# perform linkage pruning - i.e. identify prune sites
# "cichlids" is just a random name - you could use any other word, as long as you have the same thing with an added ".prune.in" in the next command
plink2 --vcf head_filtered2.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --indep-pairwise 10 1 0.1 --out cichlids

# prune and create pca
plink2 --vcf head_filtered2.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --extract cichlids.prune.in --make-bed --out headplink
```

This will produce the output files: headplink.bed, headplink.bim and headplink.fam.

# 4. Admixture

ADMIXTURE does not accept chromosome names that are not human chromosomes. We will thus just exchange the first column by 0

```
awk '{$1=0;print $0}' headplink.bim > headplink.bim.tmp
mv headplink.bim.tmp headplink.bim
```

Then run Admixture. In this case, we run it once with 3 ancestral populations, and with 6 ancestral populations.

```
/nfs/scistore03/vicosgrp/Bioinformatics_2020/BV_bioinfo2/admixture_linux-1.3.0/admixture headplink.bed 3
/nfs/scistore03/vicosgrp/Bioinformatics_2020/BV_bioinfo2/admixture_linux-1.3.0/admixture headplink.bed 6
```

This will produce the output files: headplink.3.Q and headplink.6.Q, which contain the fraction of the ancestry of each sample derived from the putative ancestral populations.

# 5. Plot results

We want to make barplots that show, for each sample, how much of their ancestry is derived from the putative 2 or 4 ancestral populations. In R:

```
pdf("ancestry.pdf")
par(mfrow=c(2,1))
par(mar=c(1,0,1,0))
col3<-c("blue", "lightblue", "darkblue")
tbl=read.table("headplink.3.Q")
barplot(t(as.matrix(tbl)),  col= col3, xlab="Individual #", ylab="Ancestry", border=NA)
col6<-c("blue", "lightblue", "darkblue", "green3", "lightgreen", "darkgreen")
tbl=read.table("headplink.6.Q")
barplot(t(as.matrix(tbl)), col= col6, xlab="Individual #", ylab="Ancestry", border=NA)
dev.off()
```