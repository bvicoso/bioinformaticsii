# Run a gene expression correlation analysis to find functional gene modules using the R package WGCNA

This pipeline can be used to find modules of co-expressed genes from a single experiment, and requires:
* a comma-delimited expression table, format: genename,sample1,..,sampleN. Extra gene information columns can be present, but are removed at the beginning of the analysis. 
The expression data can be microarray probe intensity data, or normalized log2(TPM+1) if using RNA-seq data.

In this example, I am using their test dataset, available [here](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/FemaleLiver-Data.zip)

* the R package WGCNA

To install WGCNA (on a recent version of R):

```
install.packages("BiocManager")
BiocManager::install("WGCNA")
```

## PART I: CLEAN DATA 

This follows the protocol shown [here](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/FemaleLiver-01-dataInput.R).

### Load the WGCNA package and data, and filter the data

```
library(WGCNA);
#set correct working directory
setwd("/Users/bvicoso/Documents/Presentations/Bioinfo2020/bioinfo2/WGSA/FemaleLiver-Data");
# The following setting is important, do not omit.
options(stringsAsFactors = FALSE);
#Read in the female liver data set
femData = read.csv("LiverFemale3600.csv");
# Take a quick look at what is in the data set:
dim(femData);
names(femData);
```

We need to remove columns 1:8, which are "auxiliary data" (not gene expression values; if the table has the gene names as the first column and then gene expression for each sample, remove only the first column), and transpose the data so genes are in different columns

```
datExpr0 = as.data.frame(t(femData[, -c(1:8)]));
###Add the row names (the gene names)
names(datExpr0) = femData$substanceBXH;
#and explicitly state the row (sample) names
rownames(datExpr0) = names(femData)[-c(1:8)];
```

Check for genes and samples with too many missing values:

```
gsg = goodSamplesGenes(datExpr0, verbose = 3);
gsg$allOK
```

If the last statement returns TRUE, all genes have passed the cuts. If not, we remove the offending genes and samples from the data:

```
if (!gsg$allOK)
{
  # Optionally, print the gene and sample names that were removed:
  if (sum(!gsg$goodGenes)>0) 
     printFlush(paste("Removing genes:", paste(names(datExpr0)[!gsg$goodGenes], collapse = ", ")));
  if (sum(!gsg$goodSamples)>0) 
     printFlush(paste("Removing samples:", paste(rownames(datExpr0)[!gsg$goodSamples], collapse = ", ")));
  # Remove the offending genes and samples from the data:
  datExpr0 = datExpr0[gsg$goodSamples, gsg$goodGenes]
}
```

Next, cluster the samples (in contrast to clustering genes that will come later) to see if there are any obvious outliers.

```
sampleTree = hclust(dist(datExpr0), method = "average");
# Plot the sample tree: Open a graphic output window of size 12 by 9 inches
# The user should change the dimensions if the window is too large or too small.
sizeGrWindow(12,9)
#pdf(file = "Plots/sampleClustering.pdf", width = 12, height = 9);
par(cex = 0.6);
par(mar = c(0,4,2,0))
plot(sampleTree, main = "Sample clustering to detect outliers", sub="", xlab="", cex.lab = 1.5, 
     cex.axis = 1.5, cex.main = 2)
```

You need to decide which samples to keep manually.

Choose a "height cut" value that will remove obvious outliers, for instance 15 in the example below.
We can plot the cut value as a red line in the dendogram, and if it seems to correctly remove the outlier lineages, we can replace the parameter "cutHeight" with this chosen value!

```
# Plot a line to show the proposed cut value
abline(h = 15, col = "red");
# Determine cluster under the line
clust = cutreeStatic(sampleTree, cutHeight = 15, minSize = 10)
table(clust)
# clust 1 contains the samples we want to keep.
keepSamples = (clust==1)
datExpr = datExpr0[keepSamples, ]
nGenes = ncol(datExpr)
nSamples = nrow(datExpr)
```

**The variable datExpr now contains the expression data ready for network analysis.**

NB: at this point the tutorial adds clinical/functional information for each sample, which can be correlated with the different modules, but we are skipping this since we only want to find the modules.


## PART 2 - calling modules with large datasets 

This is a simplified version of [this protocol](https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/FemaleLiver-02-networkConstr-blockwise.R)

### Set up R parameters

```
# The following setting is important, do not omit.
options(stringsAsFactors = FALSE);
# Allow multi-threading within WGCNA. This helps speed up certain calculations.
# At present this call is necessary.
# Any error here may be ignored but you may want to update WGCNA if you see one.
# Caution: skip this line if you run RStudio or other third-party R environments.
# See note above.
enableWGCNAThreads()
```

### Choose a set of soft-thresholding powers
In this section, you plot the scale-free topology fit for different power values used for soft-thresholding, so you can decide which to pick for the next section.
You should choose the lowest power value for which the scale-free topology fit index curve flattens out upon reaching a high value (e.g. in their example, a power value of 6, which has the curve flatening at roughly 0.90).

```
#we need to input the chosen power value as a parameter in the next section
powers = c(c(1:10), seq(from = 12, to=20, by=2))
# Call the network topology analysis function
sft = pickSoftThreshold(datExpr, powerVector = powers, verbose = 5)
# Plot the results:
sizeGrWindow(9, 5)
par(mfrow = c(1,2));
cex1 = 0.9;
# Scale-free topology fit index as a function of the soft-thresholding power
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
     xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
    main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
    labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.90,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5],
    xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
    main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
```

### Block-wise network construction and module detection

We can now run the actual correlation analysis! Some parameters to think about for this section:
* power - soft thresholding power (see previous section)
* minModuleSize - the minimum number of genes in a module, usually between 20 and 30
* TOMType - "unsigned" to consider negative as well as positive correlations; "signed" if only positive correlations should be used
* mergeCutHeight is the threshold for merging of modules: higher value => larger, more heteogeneous modules
* maxBlockSize - how large the largest block of genes that can handled by the reader’s computer.  A 4GB standard desktop or a laptop may handle up to 8000-10000 probes.

```
bwnet = blockwiseModules(datExpr, maxBlockSize = 4000,
                     power = 6, TOMType = "unsigned", minModuleSize = 30,
                     reassignThreshold = 0, mergeCutHeight = 0.25,
                     numericLabels = TRUE,
                     saveTOMs = TRUE,
                     saveTOMFileBase = "femaleMouseTOM-blockwise",
                     verbose = 3)

#The output of the function may seem somewhat cryptic, but it is easy to use. For example, bwnet$colors contains the module assignment, and bwnet$MEs contains the module eigengenes of the modules.
# 0 - no module assigned
#get modules and their gene counts
table(bwnet$colors)
```

### Draw modules and gene dendograms

```
# Relabel blockwise modules
bwLabels = matchLabels(bwnet$colors, moduleLabels);
# Convert labels to colors for plotting
bwModuleColors = labels2colors(bwLabels)

###check that the modules distribution still makes sense
table(bwLabels)
```

You should get the same counts as before.

```
#plot the dendograms for the two first blocks
# open a graphics window
sizeGrWindow(6,6)
# Plot the dendrogram and the module colors underneath for block 1
plotDendroAndColors(bwnet$dendrograms[[1]], bwModuleColors[bwnet$blockGenes[[1]]],
                  "Module colors", main = "Gene dendrogram and module colors in block 1",
                  dendroLabels = FALSE, hang = 0.03,
                  addGuide = TRUE, guideHang = 0.05)

#if there is a second block, you can also plot it
# Plot the dendrogram and the module colors underneath for block 2
plotDendroAndColors(bwnet$dendrograms[[2]], bwModuleColors[bwnet$blockGenes[[2]]],
                  "Module colors", main = "Gene dendrogram and module colors in block 2",
                  dendroLabels = FALSE, hang = 0.03,
                  addGuide = TRUE, guideHang = 0.05)
```




