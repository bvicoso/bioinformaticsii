# Differential expression analysis with Star / DEseq2

Star is a read mapper, but can also output read counts per genes. DEseq2 takes as input raw counts, and normalizes these to calculate adjusted p-values for differences between sets of samples. 

## Mapping and counting reads with Star

Star takes as input:

* RNA-seq reads in fastq format

* A genome in fasta format

* An annotation file in GTF format

 It will then output an alignment file as well as a read count file for each RNA-seq library.

 

### Reads

 We will use as an example the following set of paired-end reads (for each, we have a forward and reverse read file, e.g. SRR5311882_1.fastq and SRR5311882_2.fastq):

DDT-resistant (91-R): SRR5311882, SRR5311883, SRR5311884

DDT-susceptible (91-C): SRR5311881, SRR5311880, SRR5311879

 

### Indexing the genome:

We are using as an example the Drosophila melanogaster genome downloaded from the Ensembl page: http://www.ensembl.org/

(Downloads --> "Download databases" --> click on it to see all the databases, and get the DNA fasta and the GTF file)

``` 
wget ftp://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/dna/Drosophila_melanogaster.BDGP6.28.dna.chromosome.*.fa.gz
wget ftp://ftp.ensembl.org/pub/release-99/gtf/drosophila_melanogaster/Drosophila_melanogaster.BDGP6.28.99.chr.gtf.gz
#decompress the files
gzip -d *.gz

```

The first thing that you need to do is to run the indexing function of Star: this will have it create new files from the genome, which help it search it very quickly for similarity to your RNA-seq reads. 

First, make a new directory to store your indexed genome:

```
mkdir genomeDir
```

Then in a SLURM script, run:

```
#load any module here
module load star/2.6.0c
#run commands here
srun STAR --runThreadN 20 --runMode genomeGenerate --genomeDir genomeDir --genomeFastaFiles Drosophila_melanogaster.BDGP6.28.dna.chromosome.2L.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.2R.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.3L.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.3R.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.4.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.X.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.Y.fa Drosophila_melanogaster.BDGP6.28.dna.chromosome.mitochondrion_genome.fa --sjdbGTFfile Drosophila_melanogaster.BDGP6.28.99.chr.gtf --sjdbOverhang 159
```
NB: "for Illumina 2x100b paired-end reads, the ideal value is 100-1=99. In case of reads of varying length, the ideal value is max(ReadLength)-1. In most cases, the default value of 100 will work as well as the ideal value."


### Mapping and read counting: 

Once your genome is indexed, you can start running the mapping and read couting part of Star.

Check that the indexing run is finished using squeue, before putting the next script on the queue!

Create a folder for the output files:

```
mkdir StartOut
```

In a SLURM script: 

```
#load any modules here
module load star/2.6.0c
#run commands here
srun STAR --runThreadN 20 --genomeDir genomeDir --readFilesIn XXX_1.fastq XXX_2.fastq  --outFileNamePrefix StartOut/XXX  --quantMode GeneCounts
```

(Replace XXX with the SRA ID) 


### Output files

In the directory "StarOut", you will now find files ending in:
* Aligned.out.sam: these are the alignments themselves, i.e. the genomic region to which each read maps
* Log.out, Log.progress.out: the log files that contain information about the Star run
* Log.final.out: statistics about how many reads mapped and how well they did so. Always check this one!
* ReadsPerGene.out.tab: the read counts per gene, i.e. the file that we will use as input for DEseq2. It has 4 columns. From the manual:
column 1: gene ID
column 2: counts for unstranded RNA-seq
column 3: counts for the 1st read strand aligned with RNA (htseq-count option -s yes)
column 4: counts for the 2nd read strand aligned with RNA (htseq-count option -s reverse)

We will have to select the column that corresponds to the protocol used to make the RNA library. But what if you don't know what it was? Don't panic. Take one of the files, and run:
```
#sum of all counts if not stranded
cat SRR5311879ReadsPerGene.out.tab | grep -v '^N_' | awk '{sum += $2} END {print sum}'
#sum of all counts if 1st strand aligned
cat SRR5311879ReadsPerGene.out.tab | grep -v '^N_' | awk '{sum += $3} END {print sum}'
#sum of all counts if 2nd strand aligned
cat SRR5311879ReadsPerGene.out.tab | grep -v '^N_' | awk '{sum += $4} END {print sum}'
```

If your RNA library was unstranded, then column 3 and 4 should yield similar counts. If it was stranded, then the correct protocol should yield a much higher count. For instance, in our case we got:

| protocol |count|
| ------------- |:-------------:|
|unstranded| 17617055|
|Stranded 1st strand| 441124|
|Stranded 2nd strand| 17982491|

So it is likely the it was a stranded protocol, and that column 4 is the appropriate one to use. 

### Make matrix file to use as input for DEseq2

First go to your "StarOut" folder, and run:

```
mkdir deseq2
# from this tutorial: https://biocorecrg.github.io/RNAseq_course_2019/differential_expression.html
# retrieve the 4th column of each "ReadsPerGene.out.tab" file + the first column that contains the gene IDs
paste *ReadsPerGene.out.tab | grep -v "_" | awk '{printf "%s\t", $1}{for (i=4;i<=NF;i+=4) printf "%s\t", $i; printf "\n" }' > tmp
# add header: "gene_name" + the name of each of the counts file
sed -e "1igene_name\t$(ls *ReadsPerGene.out.tab | tr '\n' '\t' | sed 's/ReadsPerGene.out.tab//g')" tmp | cut -f1-7 > deseq2/raw_counts_matrix.txt
# remove temporary file
rm tmp
```



## Detection of differentially expressed genes using DEseq2

DEseq2 requires as input:
* The matrix that you just made (raw_counts_matrix.txt)
* A text file containing all the samples information (sample_sheet.txt)

### Make the sample information file (sample_sheet.txt)

The sample sheet should contain one line per sample, with the name of the sample as the first column, then further columns for other parameters to be taken into account in the analysis. In our case, we only have one parameter to consider (resistant vs susceptible), so our sample sheet will only have to columns. But you can often have more complex experiments, e.g. treatment versus control at different time points. 

Use pico to write into the file sample_sheet.txt (make sure that the samples are in the same order as in the counts table!):

```
Samplename	Status
SRR5311879	S
SRR5311880	S
SRR5311881	S
SRR5311882	R
SRR5311883	R
SRR5311884	R
```


### start the analysis in R:

```
module load R/4.0.0
R
```

In R, first create the DEseq object (again taken from https://biocorecrg.github.io/RNAseq_course_2019/differential_expression.html):


```
#make sure that you are working in the correct directory
setwd("path/to/deseq2")
# load package DESeq2 (all functions)
library(DESeq2)

# read in the sample sheet
sampletable <- read.table("sample_sheet.txt", header=T, sep="\t")
# add the sample names as row names (it is needed for some of the DESeq functions)
rownames(sampletable) <- sampletable$Samplename
# display and check that it looks as you would expect
print(sampletable)


# read in the matrix
count_matrix <- read.delim("raw_counts_matrix.txt", header=T, sep="\t", row.names=1)

	# then create the DESeq object
		# countData is the matrix containing the counts
		# sampletable is the sample sheet / metadata we created
		# design is how we wish to model the data: what we want to measure here is the difference between the treatment times
se_star_matrix <- DESeqDataSetFromMatrix(countData = count_matrix, colData = sampletable, design = ~ Status)
```

Then run the analysis (as suggested in the DEseq2 manual "getting started" section).


```
#optional: filter out genes with a sum of counts below 10
se_star_matrix <- se_star_matrix[rowSums(counts(se_star_matrix)) > 10, ]

dds <- DESeq(se_star_matrix)
resultsNames(dds) # lists the coefficients, so you know what to write for "coef=" in the next command
#Standard differential expression analysis
res <- results(dds, name="Status_S_vs_R")
write.table(res, file = "DEgenes.txt")

```

**Optional:** You may want to shrink the log-fold change for genes that have low counts or high dispersion if your data is noisy. This requires installing the Bioconductor package 'apeglm':
```
#install "apeglm" package
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("apeglm")
#to shrink log fold changes association with condition 
res <- lfcShrink(dds, coef="Status_S_vs_R", type="apeglm")
write.table(res, file = "DEgenes.txt")
```
You can view your top candidates (those with the lowest q-values):
```
#sort the results by q-value so top candidates come first
sortedres <- res[order(res$padj),]
#make table of top candidates; change "20" to however many you would like to see
top <- head(sortedres, 20)
#print the top candidates
print(as.matrix(top))
```

Or print the full list of significantly differentially expressed genes to its own table:

```
significant<-subset(res, padj<0.05)
write.table(significant, file = "SignificantDEgenes.txt")
```

Finally, let's make a PCA plot to see how the different samples relate to each other.

```
#normalize the raw counts using the variance stabilizing transformation (VST) option
vsd <- vst(dds)
#create png image with PCA plot 
pdf("PCA_star.pdf")
plotPCA(object = vsd, intgroup = "Status")
dev.off()
```
