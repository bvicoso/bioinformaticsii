# Differential expression analysis with Kallisto / Sleuth

Kallisto obtains gene expression values in TPM, while sleuth normalizes these and calculates adjusted p-values for differences between sets of samples. 

## Estimating gene expression values with Kallisto

Kallisto takes as input:

* RNA-seq reads in fastq format

* A transcriptome in fasta format

(in this case the fasta file will be compressed into gzipped .gz – that works, as Kallisto can directly use gzipped files. It also takes gzipped reads directly, although ours are decompressed.).

 It will then output expression values in Transcripts Per Million (TPM) for each RNA-seq library.

 

### Reads

 We will use as an example the following set of paired-end reads:

DDT-resistant (91-R): SRR5311882, SRR5311883, SRR5311884

DDT-susceptible (91-C): SRR5311881, SRR5311880, SRR5311879

 

### Indexing the transcriptome:

We are using as an example the Drosophila melanogaster cDNAdatasabe downloaded from the Ensembl page: http://www.ensembl.org/

(Downloads --> "Download databases" --> click on it to see all the databases)
 

The first thing that you need to do is to run the indexing function of Kallisto: this will have it create a new file from the transcriptome, which helps it search the transcripts very quickly for similarity to your RNA-seq reads. In a new SLURM script, run:

```
#load any module here
module load kallisto
#run commands here
srun kallisto index -i transcripts.idx Drosophila_melanogaster.BDGP6.28.cdna.all.fa.gz
```


### Mapping and quantification: 

Once your transcriptome is indexed, you can start running the mapping and quantification part of Kallisto. Unlike other programs, which do this in two steps, Kallisto combines them into one (and is very fast).

Check that the indexing run is finished using squeue, and that the file transcripts.idx was created, before putting the next script on the queue!

In a SLURM script: 

```
#load any modules here
module load kallisto
#run commands here
srun kallisto quant -t 16 -i transcripts.idx -o XXX -b 100 XXXt_1_short.fastq XXX_2_short.fastq
```

 (Replace XXX with the SRA ID; you should have one command per set of paired end reads) 

  

## Detection of differentially expressed genes using Sleuth

### Organize the Kallisto Results

Sleuth is an R package that uses the Kallisto output to detect differentially expressed genes. First, we need to create a folder that will contain all the Kallisto output files, as well as an information file that explains what each SRA sample corresponds to (in this case either aggressive or not aggressive).

 

Make a folder called KalResults

```
mkdir KalResults
cd KalResults
```

Let's first edit the file that contains the sample information needed by Sleuth. 

```
pico hiseq_info.txt
```
 

Paste the following information on what each of the samples corresponds to (DDT-susceptible or resistant): 

```
run_accession condition
SRR5311879 Susceptible
SRR5311880 Susceptible
SRR5311881 Susceptible
SRR5311882 Resistant
SRR5311883 Resistant
SRR5311884 Resistant
```

[This should list your samples in the order they appear when you list them (using the command ls), otherwise you will get an error!]


Make a folder for the actual Kallisto result files:

```
mkdir KalFiles 
```

Move all the Kallisto results to these folders:

```
mv ../SRR53118*[0-9] KalFiles
```

List what is in the KalFiles result (ls KalFiles):  you should have one folder of Kallisto results for each of the 6 libraries.
NB: the "mv" command works because all the results files start with SRR53118 followed by some more numbers. You may have to mv each of your results folders individually if their names are unrelated to each other.
 

### Running Sleuth to obtain differentially expressed genes

We can now use R to run Sleuth! In order to open R, type:

```
module load R/3.5.2

R
```

Notice how your terminal lines now start with " > ": this means you are giving commands to R, and not to the Unix shell.

*Once you are done with R, you can quit it by typing: quit(save="no")
[Or just type q(), press enter, and reply "n" when asked if you want to save the work space.]*

 
In R, type the following commands (you could simply paste the whole script at once, but change your working directory before doing so!):

```
#These are the R commands for running Sleuth
#First, load the pre-installed Sleuth Library
library("sleuth")
```
 

Let's tell R the directory that we want to be working in (/path/to/KalResults), as well as the directory within KalResults where we put all our Kallisto output folders (KalFiles).

 
```
#set directory of Kallisto Results (change the path to your group's directory)
base_dir <- "/path/to/KalResults"
sample_id <- dir(file.path(base_dir, "KalFiles"))
kal_dirs <- sapply(sample_id, function(id) file.path(base_dir, "KalFiles", id))
```
 
Let's provide the sample information that we put in the file hiseq_info.txt:

```
#give sample information
s2c <- read.table(file.path(base_dir, "hiseq_info.txt"), header = TRUE, stringsAsFactors=FALSE)
s2c <- dplyr::select(s2c, sample = run_accession, condition)
s2c <- dplyr::mutate(s2c, path = kal_dirs)
print(s2c)
```

At this point you should see a list of sample and conditions, as well as the list of folders that contain the Kallisto results. Check that number 1 on the first list corresponds to number 1 on the second list, and so on and so forth. If they don't your results will be meaningless! 

If everything looks OK, we can run the differential expression analysis:
 
```
#run Sleuth
so <- sleuth_prep(s2c, extra_bootstrap_summary = TRUE)
so <- sleuth_fit(so, ~condition, 'full')
so <- sleuth_fit(so, ~1, 'reduced')
so <- sleuth_lrt(so, 'reduced', 'full')
 
#Output the results to tables to be used later
#Sleuth produces two kinds of tables, one with q-values, another with the summary of expression values
results_table <- sleuth_results(so, 'reduced:full', test_type = 'lrt')
SummaryKallisto_table<- sleuth_to_matrix(so, "obs_norm", "tpm")

#print these tables to text files
write.table(results_table, file = "DEtranscripts_DDT.txt")
write.table(SummaryKallisto_table, file = "KallistoSummary_transcripts.txt")
```
 
[NB: This looks complicated! But it really just follows the recipe explained in details here: https://pachterlab.github.io/sleuth_walkthroughs/trapnell/analysis.html]

This created files with transcript expression (KallistoSummary_transcripts.txt), and differentially expressed transcripts (DEtranscripts_DDT.txt). This type of analysis is very useful when you assembled a transcriptome de novo, or you are interested in alternative spliceforms.

### Using gene information

Since we know which genes transcripts correspond to in Drosophila, let's make the most of this information and get a list of differentially expressed genes. You can do this for any transcriptome that you obtain from Ensembl (but of course you need to change the species name).
 
Let's first use the biomart function of biocLite to get the gene names corresponding to each transcript in our cDNA file:

```
#retrieve gene names from Biomart
mart <- biomaRt::useMart(biomart = "ENSEMBL_MART_ENSEMBL",  dataset = "dmelanogaster_gene_ensembl",  host = 'ensembl.org')
t2g <- biomaRt::getBM(attributes = c("ensembl_transcript_id", "ensembl_gene_id",    "external_gene_name"), mart = mart)
t2g <- dplyr::rename(t2g, target_id = ensembl_transcript_id,  ens_gene = ensembl_gene_id, ext_gene = external_gene_name)
```
 
Once that information has been provided, we can re-run Sleuth with the aggregation_column = 'ens_gene' option:

```
#run Sleuth for whole genes
so_genes <- sleuth_prep(s2c, ~condition, target_mapping = t2g, aggregation_column = 'ens_gene', gene_mode = TRUE, extra_bootstrap_summary = TRUE)
so_genes <- sleuth_fit(so_genes, ~condition, 'full')
so_genes <- sleuth_fit(so_genes, ~1, 'reduced')
so_genes <- sleuth_lrt(so_genes, 'reduced', 'full')

#Output the results to tables
sleuth_table <- sleuth_results(so_genes, 'reduced:full', 'lrt', show_all = FALSE)
write.table(sleuth_table, file = "DEgenes_DDT.txt")
kallisto_table<-sleuth_to_matrix(so_genes, "obs_norm", "tpm")

#Make sure first column are gene names
SummaryTPM <- cbind(rownames(kallisto_table), data.frame(kallisto_table, row.names=NULL))
colnames(SummaryTPM)[1] <- "gene"
write.table(SummaryTPM, file = "ExpressionSummary_DDT.txt")

#let's put together a last table that has both the qvalues and the expression values
final<-merge(sleuth_table, SummaryTPM, by.x="target_id", by.y="gene")
write.table(final, file = "DEgenes_expression_DDT.txt")
```


Sleuth allows you to draw expression plots for specific genes. Let's draw one for the top candidate that we got. First, let's have a look at the beginning of the Sleuth table:

```
head(sleuth_table)
```

You should be able to see the 6 genes with the lowest qvalues. Let's make a plot for the first:

```
#tell R to produce a pdf file called Gene_1.pdf
pdf("Gene_1.pdf", width=14, height=7)
#use Sleuth's plotting command (change to your top result)
plot_bootstrap(so_genes, "FBgnXXX", units = "scaled_reads_per_base", color_by = "condition")
#tell R that your pdf is ready - it won't produce the file without this final command!
dev.off()
```
