# Gene ontology analysis of a nonmodel organism with Panzer

Gene ontologies are obtained with Panzer. We can also compare the GOs of differentially expressed transcripts to those of the whole transcriptome to see if some categories are enriched.

## Requirements

* your reference transcriptome
* dna2proteins.py script (available [here](https://github.com/prestevez/dna2proteins), or on the bioinformaticsII folder).
* Optional: list of differentially expressed genes, if you are checking for GO enrichment among them. This should contain only one transcript name per line, e.g.:

```
transcript1
transcript2
transcript3
```
For instance, I obtained this from the Kallisto/Sleuth output with the command:
```
cat DEtranscripts_Artemia.txt | awk '($3<0.005)' | cut -f 2 -d " " | perl -pi -e 's/\"//gi' | sort > DEtranscripts_Artemia_clean.txt
```


## Produce fasta of proteins

The annotation server that we will use requires protein sequences.

The script used  [here](https://github.com/prestevez/dna2proteins) to obtain the protein sequences was produced for a python course, so it does not seem to have been super well tested. Maybe we should test it or eventually replace this step with other dna2protein algorithm, but hopefully it will work for now! 
The other issue is that it only keeps open reading frames that start with an "M". This may miss ORFs of partial transcripts.


```
python /nfs/scistore03/vicosgrp/Bioinformatics_2020/BV_bioinfo2/dna2proteins.py -i Aata500.fa -o Aata500_prot.fa -p
```

This produces that Aata500_prot.fa file, which contains the longest possible protein per transcript (of the 6 reading frames) in fasta format.

## Obtain list of GOs for the whole transcriptome

We use the online version of Panzer, which works to up to 10Mb of data. If your protein dataset is too large, break the fasta into smaller files.

```
http://ekhidna2.biocenter.helsinki.fi/sanspanz/
```
Upload your protein fasta file (which you obtained in the previous section), and use the Batch queue option. This will take ~30mins for 20000 proteins.

When the job is done running, right-click to save the "GO prediction details" file (GO.txt). This contains all the predicted GOs for your transcripts.

## Check for GO enrichment among differentially expressed genes

First, let's clean up the GO file so it only has two columns: transcriptname, GO
(the same transcript can be listed many times, if it has multiple GOs)
And let's remove the first line, which has column names.

```
cat GO.txt | cut -f 1,3 | grep -v '^qpid' | awk '{print $1"\tGO:"$2}' | sort  > GO_clean.txt 
```
This set of GOs will be used as our background reference in the enrichment analysis. We 'sort' the file because this makes it easier to create the second GO file with differentially expressed genes (see next step).

Let's make a second file with transcripts and GOs, that only contains the differentially expressed genes. This will be used as out 'test' file in the GO enrichment analysis.

```
sort DEtranscripts.txt | join -t "$(echo -e "\t")"  /dev/stdin GO_clean.txt > DEtranscripts_GO.txt
```


Now we can use R to compare the frequencies of GOs in our differentially expressed genes and in the background reference (the whole transcriptome). 


```
####Beatriz's R script to look for GO enrichment - May 2020

#V2: fixed counts to account for the fact that the gene set to be tested is a subset of the reference, and should be subtracted from it to make true 2x2 contingency tables for the Fisher's exact tests

#set your working directory
setwd("~/Documents/Presentations/Bioinfo2020/bioinfo2/")

#read in background reference and test dataset
#each should be in tab-separated format, col1 = transcript, col2 = GO
background<-read.table("GO_Aata_clean.txt", sep="\t")
test<-read.table("DEtranscripts_GO.txt", sep="\t")
head(background)

#count the number of times that each GO appears in each dataset
 t_test<-as.data.frame(table(test$V2))
 t_background<-as.data.frame(table(background$V2))
 bigtable<-merge(t_background, t_test, by.x="Var1", by.y="Var1", all=T)
bigtable[is.na(bigtable)] <- 0
names(bigtable) = c("GO", "Freq.BG", "Freq.DE")
bigtable$Freq.BG<-bigtable$Freq.BG-bigtable$Freq.DE
head(bigtable)

#make 2x2 contingency for each gene ontology
#First count total number of reference and DE transcripts in the GO files
uniquede<-unique(test$V1)
sumde<-sum(as.data.frame(table(uniquede))$Freq)
uniqueref<-unique(background$V1)
sumref<-sum(as.data.frame(table(uniqueref))$Freq)
#remove DE genes from reference count
sumref<-sumref-sumde


#Number of reference transcripts that do not have this GO
bigtable$bg_other<-sumref-bigtable$Freq.BG
#Number of DE transcripts that do not have this GO
bigtable$de_other<-sumde-bigtable$Freq.DE
#Proportion of this GO in DE dataset relative to background
bigtable$enrich<-(bigtable$Freq.DE/(bigtable$Freq.DE+bigtable$de_other))/(bigtable$Freq.BG/(bigtable$Freq.BG+bigtable$bg_other))

#Create new table for GO enrichment results
newtable<-bigtable[1,]
newtable$fisher<-"1"
#newtable$chisq<-"1"
newtable$padjust<-"1"

#calculate p-value of fisher's exact for each GO, and correct with 
for (i in 1:dim(bigtable)[1]) {
   temp<-bigtable[i,]
  temp$fisher<-fisher.test(matrix(c(temp$Freq.DE, temp$Freq.BG, temp$de_other, temp$bg_other),ncol=2), simulate.p.value=TRUE)$p.value
# temp$chisq<-chisq.test(matrix(c(temp$Freq.DE, temp$Freq.BG, temp$de_other, temp$bg_other),ncol=2),correct=T)$p.value
temp$padjust<-p.adjust(temp$fisher, "BH", n = dim(bigtable)[1])
newtable<-rbind (newtable, temp)
if (temp$fisher<0.01 & temp$enrich>1){
print (temp)

}
 }

#get rid of dummy first line
newtable = newtable[-1,]
#put low p-values first
newtable <- newtable[order(as.numeric(newtable$fisher)),]

write.table(newtable, file = "GOanalysis.txt", quote=F, sep="\t", append=F)
``` 

The output file GOanalysis.txt has the following columns:
* GO: GO term
* Freq.BG: number of reference transcripts that were assigned to this GO
* Freq.DE: number of DE transcripts that were assigned to this GO
* bg_other: number of reference transcripts that were not assigned to this GO
* de_other: number of DE transcripts that were not assigned to this GO
* enrich: the enrichment of this GO in the DE dataset relative to its background frequency (>1 = enrichment)
* fisher: p-value obtained from Fisher's exact test (<0.05 = significant, but NOT CORRECTED for multiple testing)
* padjust: p-value adjusted for multiple testing, using a false discovery rate approach (<0.05 = significant)

You can check what your GOs of interest are [at EBI's QuickGO database](https://www.ebi.ac.uk/QuickGO/):
```
https://www.ebi.ac.uk/QuickGO/
```