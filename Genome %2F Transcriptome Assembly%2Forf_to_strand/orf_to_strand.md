# Pipeline to quickly infer strand specificity of RNA-seq libraries

Step 1: run Trinity assembly with option  --SS_lib_type RF (paired end) / --SS_lib_type F (single end)
Step 2: Check if transcripts have a longer orf in forward (RF correct) or reverse (FR correct) strand, or in neither strand (unstranded RNA-seq)
Step 2 (single-end): Check if transcripts have a longer orf in forward (F correct) or reverse (R correct) strand, or in neither strand (unstranded RNA-seq)

## Step 1: run trinity assembly

```
#paired end:
Trinity --seqType fq --left $run_1.fastq --right $run_2.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
#single end:
Trinity --seqType fq --single $run_1.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
```


## Step 2: run ORFtoStrand pipeline 

```
# Keep only long transcripts (make sure to use the output name trinity_genes_1000.fasta or change the final R script)
module load fafilter/v.357
faFilter -minSize=1000 trinity_genes.fasta trinity_genes_1000.fasta 
perl orftostrand1.pl trinity_genes_1000.fasta 

#Get distribution of length for forward frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds

#Get distibution of length for reverse frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds2

# compare them using R
module load R
~/scripts/ORF_to_strand$ Rscript orftostrand2.Rscript 

```
