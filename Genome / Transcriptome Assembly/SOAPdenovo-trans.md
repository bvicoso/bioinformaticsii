# Transcriptome assembly with SOAPdenovo-trans

In order to SOAPdenovo-trans to assemble the transcriptome, you need:

* A configuration text file, that will give SOAPdenovo-trans the information it requires about the sequencing libraries.

* A SLURM script, which will allow us to run the transcriptome assembly on the Cluster queue.

Both of these can be created and edited using pico.

 

Let's start with the configuration file: 
```
pico soapconfig.txt
```

Add the configuration details to it (text below). You should change parts that are in bold to match the information of your libraries (insert size, name of fastq files - use the cleaned ones!). If you are using multiple libraries, then everything from "[LIB]" to the end of the configuration file should be repeated with the corresponding information for each library.

```
#maximal read length
max_rd_len=101
[LIB]
#maximal read length in this lib
rd_len_cutof=101
#average insert size
avg_ins=200
#if sequence needs to be reversed
reverse_seq=0
#in which part(s) the reads are used
asm_flags=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#fastq file for read 1
q1=SRAID_1_paired.fastq
#fastq file for read 2 always follows fastq file for read 1
q2=SRAID_2_paired.fastq
```

Once you have saved this file, you can create and edit the SLURM script:
```
pico SOAPdenovo-trans.sh 
```
And add the following text to it:

```
#!/bin/bash
#
#we first start by giving the cluster a few options on the job: which queue to put it into (nick), and the name we are giving it
#SBATCH --partition=defaultp
#SBATCH --job-name=assembly
#SBATCH --output=assembly
#
#Number of CPU cores to use within one node
#SBATCH -c 8
#
#Define the number of hours the job should run.
#Maximum runtime is limited to 10 days, ie. 240 hours
#SBATCH --time=96:00:00
#
#Define the amount of RAM used by your job in GigaBytes
#In shared memory applications this is shared among multiple CPUs
#SBATCH --mem=60G
#
#Send emails when a job starts, it is finished or it exits
#SBATCH --mail-user=youremail@ist.ac.at
#SBATCH --mail-type=ALL
#
#Pick whether you prefer requeue or not. If you use the --requeue
#option, the requeued job script will start from the beginning,
#potentially overwriting your previous progress, so be careful.
#For some people the --requeue option might be desired if their
#application will continue from the last state.
#Do not requeue the job in the case it fails.
#SBATCH --no-requeue
#
#Do not export the local environment to the compute nodes
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV
#
#Set the number of threads to the SLURM internal variable
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#
#load any module you need here
module load SOAPdenovoTrans
#run commands on SLURM's srun
srun SOAPdenovo-Trans-31mer all -s soapconfig.txt -K 31 -o TranscriptAssembly -p 16 1> TranscriptAssembly.stdout 2> TranscriptAssembly.stderr
```
