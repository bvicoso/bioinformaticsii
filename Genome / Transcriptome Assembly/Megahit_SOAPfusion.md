# Genome assembly with Megahit and SOAPfusion

Megahit is a multi-k-mer assembler. While it works very fast, is memory-efficient, and usually produces long contigs, it does not do the scaffolding step (in which the paired-end information is used). For this, we use SOAPfusion, a module of SOAPdenovo.

## Megahit assembly

Megahit can simply be dowloaded locally. The following command uses a copy of the software that is in /nfs/scistore03/vicosgrp/Bioinformatics_2019/ , and which should work for everybody. 

For assembly of a single library, use:
```
#run commands here
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/megahit_v1.1.4_LINUX_CPUONLY_x86_64-bin/megahit -1 Library1_1.fastq -2 Library1_2.fastq -o Assembly_mega
```

For multiple libraries:
```
#run commands here
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/megahit_v1.1.4_LINUX_CPUONLY_x86_64-bin/megahit -1 Library1_1.fastq -2 Library1_2.fastq -1 Library2_1.fastq -2 Library2_2.fastq -1 Library3_1.fastq -2 Library3_2.fastq -o Assembly_mega
```

The assembly fasta file "final.contigs.fa" will be in the directory "Assembly_mega".

## Scaffolding of contigs using SOAPfusion

Create a new "SOAPfusion" folder, link the contigs to it, and go to that folder:
```
mkdir SOAPfusion
ln -s Assembly_mega/final.contigs.fa SOAPfusion
cd SOAPfusion
```

Create a configuration file for SOAPdenovo with pico:
```
pico soapconfig.txt
```

And add to it the following text (change the read length, insert size, and name of fastq files to yours):
```
#maximal read length
max_rd_len=151
[LIB]
avg_ins=475
reverse_seq=0
asm_flags=2
#in which order the reads are used while scaffolding
rank=1
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=Library1_1.fastq
q2=Library1_2.fastq
[LIB]
avg_ins=475
reverse_seq=0
asm_flags=2
#in which order the reads are used while scaffolding
rank=1
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=Library2_1.fastq
q2=Library2_2.fastq
[LIB]
avg_ins=475
reverse_seq=0
asm_flags=2
#in which order the reads are used while scaffolding
rank=1
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=Library3_1.fastq
q2=Library3_2.fastq
```

Once you have the configuration file, you can run SOAPfusion. You need to chose a k-mer value (here, a kmer of 41 is used). Run in a SLURM script:
```
#load any module you need here
module load SOAPdenovo2
#run commands on SLURM's srun
srun SOAPdenovo-fusion -D -s soapconfig.txt -p 30 -K 41 -g k41_scaffolds -c final.contigs.fa
srun SOAPdenovo-127mer map -s soapconfig.txt -p 30 -g k41_scaffolds
srun SOAPdenovo-127mer scaff -p 30 -g k41_scaffolds
```