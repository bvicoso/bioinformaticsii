# Transcriptome assembly with Trinity

## Trinity assembly (NB: can be run directly with a Trimmomatic step)

```
#load modules here
module load trinity/2.8.4 
#run commands here
srun Trinity --seqType fq --left reads_1.fq --right reads_2.fq --output AssemblyFolder --CPU 6 --max_memory 20G 
```



## With an extra step to clean reads with Trimmomatic:

```
#load modules here
module load trinity/2.8.4 
#run commands here
srun Trinity --seqType fq --left reads_1.fq --right reads_2.fq --output AssemblyFolder --CPU 6 --max_memory 20G --trimmomatic 
```
