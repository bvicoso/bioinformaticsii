
# Genome assembly using SOAPdenovo

 We will use SOAPdenovo to assemble the genome. In order to use it, you need:

* A configuration text file, that will give SOAPdenovo the information it requires about the sequencing libraries.

* A SLURM script that will allow us to run the genome assembly on the Cluster queue.

Both of these can be created and edited using pico.

*** 

Let's first create and edit the configuration file using pico: 
```
pico soapconfig.txt
```
 Add the configuration details to it (text below). You need to do this for each pair of fastq reads: everything from [LIB] to the end of the file should be repeated if you have two read libraries. You should change parts that are in bold to match the information of your libraries (insert size, name of fastq files - use the cleaned ones!). 

```
# read length
max_rd_len=101
[LIB]
#average insert size
avg_ins=500
#if sequence needs to be reversed
reverse_seq=0
#in which part(s) the reads are used
asm_flags=3
#use only first 100 bps of each read
rd_len_cutoff=101
#in which order the reads are used while scaffolding
rank=1
# cutoff of pair number for a reliable connection (at least 3 for short insert size)
pair_num_cutoff=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#a pair of fastq file, read 1 file should always be followed by read 2 file
q1=SRAID_1_paired.fastq
q2=SRAID_2_paired.fastq
```

 ***

Once you have saved this file, you can create and edit the SLURM script:
```
pico SOAPdenovo.sh 
```
And add the following text to it:

 
```
#!/bin/bash
#
#-------------------------------------------------------------
#running a shared memory (multithreaded) job over multiple CPUs
#-------------------------------------------------------------
#
#SBATCH --partition=bigmem
#SBATCH --job-name=assembly
#SBATCH --output=assembly_output
#
#Number of CPU cores to use within one node
#SBATCH -c 8
#
#Define the number of hours the job should run.
#Maximum runtime is limited to 10 days, ie. 240 hours
#SBATCH --time=96:00:00
#
#Define the amount of RAM used by your job in GigaBytes
#In shared memory applications this is shared among multiple CPUs
#SBATCH --mem=150G
#
#Send emails when a job starts, it is finished or it exits
#SBATCH --mail-user=youremail@ist.ac.at
#SBATCH --mail-type=ALL
#
#Pick whether you prefer requeue or not. If you use the --requeue
#option, the requeued job script will start from the beginning,
#potentially overwriting your previous progress, so be careful.
#For some people the --requeue option might be desired if their
#application will continue from the last state.
#Do not requeue the job in the case it fails.
#SBATCH --no-requeue
#
#Do not export the local environment to the compute nodes
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV
#
#Set the number of threads to the SLURM internal variable
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#
#load modules here:
module load SOAPdenovo2
#run commands using SLURM's srun
srun SOAPdenovo-63mer all -s soapconfig.txt -K 31 -o Assembly -p 16 2>assembly.stdout 1>assembly.stderr
```


 

[NB: due to the large size of the reads file, the assembly requires too much RAM for the normal nodes, and we have to use the partition bigmem instead of defaultp.]

 

