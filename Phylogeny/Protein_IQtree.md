# Protein Phylogeny using muscle, Gblocks and IQtree

This pipeline takes as input:
* A fasta file containing multi-species sequences for a single protein: Protein.fa

## Align the protein sequences using muscle

Let's first align the proteins.

```
module load muscle/3.8.1551 
srun muscle -in Protein.fa -out ProteinAlignment.fa

```

## Clean the alignment with Gblocks

If proteins are very diverged, parts of the alignment may not be very informative, as they will be too noisy to show a phylogenetic signal. Gblocks filters the alignment to keep only regions that are fairly conserved between the different proteins.

```
module load gblocks/0.91b
srun Gblocks -t=p ProteinAlignment.fa
```

## Obtain maximum likelihood tree with IQtree

IQtree is a fast ML phylogeny inference software. The following command runs it with default parameters, and 100 bootstraps.

```
module load iqtree/1.5.5 
srun iqtree -s ProteinAlignment.fa-gb -b 100
```

The main output file will be **ProteinAlignment.fa-gb.treefile**, which will contain the tree in text format. 

You can view and edit it at [the Interactive Tree Of Life website](https://itol.embl.de/upload.cgi). Just paste the text into the "Tree text" box, or upload the ".treefile" text file!