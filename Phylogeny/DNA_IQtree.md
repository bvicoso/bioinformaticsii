# Maximum likelihood tree from DNA sequences

Requirements:
* fasta file with DNA sequences (sequences.fasta)
* aligner (translatorX for coding sequences, Mafft for non-coding sequences)
* Gblocks to remove unreliable parts of the alignment
* IQtree to produce the tree

## Option 1: non-coding DNA sequences

### Produce the alignment from fasta file

According to [this recent review paper](https://rnajournal.cshlp.org/content/early/2020/01/31/rna.073015.119.abstract), Mafft seems to do a good job at aligning non-coding sequences. 

```
module load mafft
srun mafft --thread 10 sequences.fasta > alignment.fasta
```

### Clean alignment with Gblocks

Let's remove unreliable parts of the alignment with Gblocks.

```
module load gblocks/0.91b 
srun Gblocks alignment.fasta -t=d 
```

This produces the clean alignment file: **alignment.fasta-gb**

(NB: I had an error that my sequence names were too long, because the fasta files obtained from NCBI often have long descriptions in the title line. I fixed this with: "perl -pi -e 's/ .*//gi' alignment.fasta", which removes everything after a space until the end of the line.)


### Phylogeny inference with IQtree

Let's infer the phylogeny from this alignment using IQtree, a fast maximum-likelihood method:

```
module load iqtree/1.6.12
srun iqtree -b 100 -s alignment.fasta-gb
```

This runs IQtree with 100 bootstraps, and outputs the final tree to:
* Maximum-likelihood tree: **alignment.fasta-gb.treefile**

This is just a text file, but it can be visualized and edited at [ITOL](https://itol.embl.de/upload.cgi).


## Option 2: protein-coding DNA sequences

In this case, we want to make sure to use a codon-aware aligner, and to run Gblocks in codon mode.

### Produce the alignment from fasta file

### Clean alignment with Gblocks

### Phylogeny inference with IQtree


