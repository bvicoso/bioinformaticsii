# Using genomic coverage to find X-linked scaffolds

We will use Drosophila as an example for this, as we can check that the pipeline worked. 

Files needed:
* genome assembly
* male and female genomic reads

## Part 1: Find X

### Download Files

In your directory on the cluster, first download the draft genome assembly used in Vicoso & Bachtrog 2015:
```
#download with wget
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/014/345/GCA_001014345.1_ASM101434v1/GCA_001014345.1_ASM101434v1_genomic.fna.gz

# decompress the gzipped file 
gzip -d GCA_001014345.1_ASM101434v1_genomic.fna.gz
```

In order to download the reads, you need to use the SRA-toolkit (version 2.8.1-3). This is the official program provided by NCBI (ncbi.nlm.nih.gov), where the read are hosted, to download data from their website. 

Using pico, make a new SLURM script (for instance, "pico download.sh"). In the module load / run commands section of your script, add:

```
#load any module you need here
module load SRA-Toolkit/2.8.1-3

#run commands on SLURM's srun
#female DNA
srun fastq-dump --skip-technical --readids --split-files SRR1738160
#male DNA 
srun fastq-dump --skip-technical --readids --split-files SRR1738161
```

### Map reads

We will use bowtie2 for mapping the reads to the genomic scaffold. This requires two steps:
1. The genome needs to be "indexed" (formatted in a way that makes it possible for bowtie2 to search it)
2. We map the female and male reads to the indexed genome. The output of this will be a SAM alignment file: it will show, for each read, where in the genome and how well it maps.

Both steps need to be done using SLURM scripts, to which you add (increase the RAM to 30GB and the number of CPUs to 50):
```
module load  bowtie2/2.3.4.1
#Run Bowtie
srun bowtie2-build  GCA_001014345.1_ASM101434v1_genomic.fna DroGenome
srun bowtie2 -x DroGenome -1 femalereads_1.fastq -2 femalereads_2.fastq --end-to-end --sensitive -p 50 -S Female.sam
srun bowtie2 -x DroGenome -1 malereads_1.fastq -2 malereads_2.fastq --end-to-end --sensitive -p 50 -S Male.sam

```

### Estimate male and female coverage


```
module load soap/coverage

#Run SOAPcov (Version: 2.7.7)
srun soap.coverage -sam -cvg -i Female.sam -onlyuniq -p 50 -refsingle GCA_001014345.1_ASM101434v1_genomic.fna -o Female.soapcov
srun soap.coverage -sam -cvg -i Male.sam -onlyuniq -p 50 -refsingle GCA_001014345.1_ASM101434v1_genomic.fna -o Male.soapcov
#Remove SAM files
srun rm Male.sam Female.sam
```

We need to clean up the files and merge the female and male data before processing it in R:

```
cat Female.soapcov | awk '{print $1, $2, $4}' | perl -pi -e 's/:.*\// /gi' | perl -pi -e 's/Depth://gi' | sort > Female.soapfinal
cat Male.soapcov | awk '{print $1, $2, $4}' | perl -pi -e 's/:.*\// /gi' | perl -pi -e 's/Depth://gi' | sort > Male.soapfinal

join Female.soapfinal Male.soapfinal | perl -pi -e 's/^\n.*//gi' > FemaleMale.soapfinal
```

### Assign scaffolds to the X or to the autosomes based on coverage

This part is done using R, either on the cluster, or on any computer. To open R on the cluster:
```
module load R
R
```

Once R is open we can start the analysis. First, we load and format the table:

```
### First tell R which directory to work in
setwd("~/Documents/RandomScience/Luisa/")

#############################################
#### Part 1: load and format the table

### Load the coverage file
cov<-read.table("FemaleMale.soapfinal", head=F, sep=" ")
### Give names to the different columns so we know what they are
colnames(cov)<-c("Scaffold", "Length", "Fcov", "L2", "Mcov")
### Use "head" to check that the table looks good
head(cov)

### calculate the log2 of the M/F coverage for each scaffold
cov$ratio<-log2(cov$Mcov/cov$Fcov)
### Use "head" to check that the calculation is working
head(cov)
```

We now have a table that has not only the female and male coverage, but also the log2(ratio between them), which we will visualize and use to classify scaffolds as X-linked or autosomal. Let's start by visualizing the distribution of this Log2(ratio).

```
#############################################
#### Part 2: Draw distribution and plot expected location of autosomes and X

### Draw the density plot of the ratio of M:F coverage

### First create the pdf file to print into
pdf("Density.pdf", width=8, height=8)

### Plot the density of the ratio of M:F coverage
par(mfrow=c(1,1))
plot(density(cov$ratio, adjust=2, na.rm = T), xlim=c(-2,2), main="", xlab="Log2(Mcov/Fcov)", cex.axis=1.5, cex.lab=1.5)
print(density(cov$ratio, adjust=2, na.rm = T))

### Add vertical line where we expect the autosomal peak to be (median of the M:F coverage)
abline(v=median(cov$ratio, na.rm=T), col="red")
### Add vertical line where we expect the X peak to be (median of the M:F coverage)
abline(v=median(cov$ratio, na.rm=T)-1, col="red")
### Add dotted vertical line where we will split the data into X and autosome
abline(v=median(cov$ratio, na.rm=T)-0.5, col="red", lty=2)

#our PDF is ready - you have to tell R or you won't be able to open it!
dev.off()
```

We can open the pdf and see if the Log2(ratio) has a bimodal distribution, as expected if you get a peak for the autosomes and one for the X. The autosomal peak should be close to the median(Log2(ratio)) of the sample as long as not too many scaffolds are X-derived.

If this looks right (and the dotted line is separating the two peaks reasonably well), we can use the same approach to assign scaffolds to the X or autosomes.

```
#############################################
#### Part 3: assign each scaffold to the autosomes or X

### Select X scaffolds
xchr<-subset(cov, ratio<(median(cov$ratio, na.rm=T)-0.5))
### Add "X" to a "chromosome" column
xchr$chromosome<-"X"
### Check that it worked
head(xchr)

### Select autosomal scaffolds
achr<-subset(cov, ratio>(median(cov$ratio, na.rm=T)-0.5))
### Add "A" to a "chromosome" column
achr$chromosome<-"A"
### Check that it worked
head(achr)

### Combine the two into a single table, and output it into a new table
finalcov<-rbind(xchr, achr)
write.table(finalcov, file="finalcov.txt", row.names=F, quote=F)
```

## Part 2: Check if X-assignment is correct

Since we are using D. melanogaster, which is well characterized, we can compare our Z/A assignment to the real genomic location. To do so we will:
- download gene sequences, and extract their genomic location from the fasta file.
- "map" these sequences to the genome assembly that we have been using, and based on which scaffold they map to, classify them as "X" or "A".
- Check that X-linked genes are indeed classified as "X", while autosomal genes are classified as "A".

### Download gene sequences and make table with their genomic locations

We will download CDS (coding sequences) for all D. melanogaster genes, as these are easy to map. One issue is that there can be multiple coding sequences for the same gene, and we only want one. So in the next steps, we will take these coding sequences and: 
- select only the longest coding sequence for each gene, which will be stored in the new file dmelLongestCDS.fasta
- extract the gene name and its genomic location, which will be stored in the table called Dmel_longestCDS_chromosome.joinable

Download CDS: 

```
wget ftp://ftp.ensembl.org/pub/release-100/fasta/drosophila_melanogaster/cds/Drosophila_melanogaster.BDGP6.28.cds.all.fa.gz
gzip -d Drosophila_melanogaster.BDGP6.28.cds.all.fa.gz
```

Get file with only longest CDS:

```
cat Drosophila_melanogaster.BDGP6.28.cds.all.fa | perl -pi -e 's/.*gene:/>/gi' | perl -pi -e 's/ .*//gi' | perl -pi -e 's/\n/ /gi' | perl -pi -e 's/>/\n>/gi' | sort | perl -pi -e 's/ /\n/gi' | perl -pi -e 's/^\n//gi' > DmelCDS
perl /nfs/scistore18/vicosgrp/bvicoso/scripts/getlongestCDS_v2.pl DmelCDS
```

This produces the output file DmelCDS.longestCDS

Get gene locations:

```
cat Drosophila_melanogaster.BDGP6.28.cds.all.fa | grep '>' | perl -pi -e's/.*BDGP6\.28://gi' | perl -pi -e 's/:.*?gene:/ /gi' | awk '{print $2, $1}' | sort | uniq > Dmel_longestCDS_chromosome.joinable 
```

### Map gene sequences to the genome assembly

Now that we have the clean CDS file DmelCDS.longestCDS, we can see where each of the genes are in our initial genome assembly. To do this, we use a mapping program called Blat. 

In a SLURM script, run:
```
module load blat
srun blat GCA_001014345.1_ASM101434v1_genomic.fna DmelCDS.longestCDS Dmel_CDS_vs_Genome.blat
```

Right now, the same gene can map to multiple locations in the genome (because it maps to its real location, but also to any other gene that it is similar to). Let's keep only the best mapping location for each gene:

```
sort -k 10 Dmel_CDS_vs_Genome.blat > Dmel_CDS_vs_Genome.sorted
perl /nfs/scistore18/vicosgrp/bvicoso//scripts/2-besthitblat.pl Dmel_CDS_vs_Genome.sorted
```

The file Dmel_CDS_vs_Genome.sorted.besthit is a shortened version of the original Blat results file: now each gene only shows up once!

Let's now pick only the gene and scaffold locations:

```
cat Dmel_CDS_vs_Genome.sorted.besthit | cut -f 10,14 | sort > DmelCDS_BadScaffold.txt
```

## Merge and compare 

So we now have the three tables we need for our final comparison:
- Dmel_longestCDS_chromosome.joinable: Gene, True genomic location
- DmelCDS_BadScaffold.txt: Gene, Bad genome scaffold
- finalcov.txt: Bad scaffold, X/A assignment # SORT THIS FILE BEFORE PROCEEDING

Let's merge them all:
```
sort DmelCDS_BadScaffold.txt | join /dev/stdin Dmel_longestCDS_chromosome.joinable | awk '{print $2, $1, $3}' | sort | join /dev/stdin finalcov.txt | head
```

If that worked, output it to a file name of your choice. 

In R, you can then plot the number of times that a gene assigned to the X has the X and other chromosomes as its true location, and the same for genes assigned as "A" based on their coverage:

```
### First tell R which directory to work in
setwd("~/Documents/RandomScience/Luisa/")

#############################################
#### Part 1: load and format the table

### Load the coverage file
cov<-read.table("finalfine.txt", head=F, sep=" ")
### Give names to the different columns so we know what they are
colnames(cov)<-c("Scaffold", "Gene", "TrueLoc", "Length", "Fcov", "L2", "Mcov", "ratio", "chromXA")
### Use "head" to check that the table looks good
head(cov)


#############################################
#### Part 2: compare true and infered locations

#Count the true locations of genes that are assigned as X or A based on coverage
tableX<-table(subset(cov, chromXA=="X")$TrueLoc)
tableA<-table(subset(cov, chromXA=="A")$TrueLoc)

#Plot
par(mfrow=c(1,2))
barplot(tableX, col="red")
barplot(tableA, col="blue")
```
