# Drop-Seq/10x single cell analysis with Salmon/Alevin

Just as bulk RNA-seq data can be analysed by mapping to the genome or by mapping to transcripts, new pipelines that quickly map single cell data to transcripts and directly output counts have been created, such as:
https://salmon.readthedocs.io/en/latest/alevin.html

In this case, we need:
- the split paired-end fastq file (where read_1.fq contains the cell barcode and unique molecular identified)
- the transcriptome
- transcript to gene map file, a tsv (tab-separated) file — with no header, containing two columns mapping of each transcript present in the reference to the corresponding gene (the first column is a transcript and the second is the corresponding gene).


We should be able to run it with the following command:

```
#Index transcriptome (both protocols)
module load salmon/0.11.0 
srun salmon index -t transcripts.fa -i transcripts_index -k 31
```
*[From their manual: We find that a k of 31 seems to work well for reads of 75bp or longer, but you might consider a smaller k if you plan to deal with shorter reads. Also, a shoter value of k may improve sensitivity even more when using the –validateMappings flag. So, if you are seeing a smaller mapping rate than you might expect, consider building the index with a slightly smaller k.]*

```
#Drop-seq
srun salmon alevin -l ISR -1 cb.fastq.gz -2 reads.fastq.gz --dropseq  -i transcripts_index -p 10 -o alevin_output --tgMap txp2gene.tsv

#10x single cell
srun salmon alevin -l ISR -1 cb.fastq.gz -2 reads.fastq.gz --chromium  -i transcripts_index -p 10 -o alevin_output --tgMap txp2gene.tsv
```
Output explanation from their manual:

*Typical 10x experiment can range form hundreds to tens of thousand of cells – resulting in huge size of the count-matrices. Traditionally single-cell tools dumps the Cell-v-Gene count matrix in various formats. Although, this itself is an open area of research but by default alevin dumps a per-cell level gene-count matrix in a binary-compressed format with the row and column indexes in a separate file.*

A typical run of alevin will generate 4 files:

* quants_mat.gz – Compressed count matrix.
* quants_mat_cols.txt – Column Header (Gene-ids) of the matrix.
* quants_mat_rows.txt – Row Index (CB-ids) of the matrix.
* quants_tier_mat.gz – Tier categorization of the matrix.