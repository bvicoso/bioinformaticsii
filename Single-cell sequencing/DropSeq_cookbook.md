# Drop-seq single cell RNA-seq data analysis (following the Drop-seq cookbook)

The Drop-seq protocol produces reads that have the following structure:

- they are paired end

- the first read is 20 base-pairs long. The first 12 base-pairs are the cell barcode, the next 8 the unique molecular identifier.

-the second read is usually 50 bps long, and contains the actual transcript sequence.

![dropseq](/images/dropseqanalysis.jpg)


The analysis has two major steps:

1. Preprocessing of the reads and alignment to the genome

2. Extraction of digital gene expression

The detection of cell types and exploration of the data can then be done using Seurat.

For the first two parts, we can follow the drop-seq cookbook. The updated version is [here](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Single-cell%20sequencing/Drop-seq_Alignment_Cookbook__1_.pdf) (somehow I could not find it directly on their website).

We are  running the pipeline on the cluster with a test dataset:

https://www.ncbi.nlm.nih.gov/sra/SRX4180400[accn]

(Drosophila melanogaster eye disk)

## Part 1: Drop-seq read preprocessing and alignment

### Software, reads and other files

The Drop-Seq toolset is here:

```
/nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/
```

We also need to load the following modules:

```
module load SRA-Toolkit/2.8.1-3
module load java
module load picard/2.18.2
module load star/2.6.0c
```

Download reads

```
#load any module you need here
module load SRA-Toolkit/2.8.1-3
#run commands on SLURM's srun
#We are now NOT SPLITTING forward and reverse reads
srun fastq-dump --skip-technical --readids SRR7276817
```

Raw reads from the sequencer must be converted into a Picard­queryname­sorted BAM file for each library in the sequencer run (I needed 100GB of RAM)

```
srun java -jar $PICARD FastqToSam F1=SRR7276817.fastq O=SRR7276817.bam SO=queryname SM=flyeyedisk
```

Download genome and GTF annotation files, and create metadata files needed (this part all worked with 10GB of RAM):

fasta: The reference sequence of the organism. Needed for most aligners.

```
#download
wget ftp://ftp.ensembl.org/pub/release-96/fasta/drosophila_melanogaster/dna/Drosophila_melanogaster.BDGP6.22.dna.toplevel.fa.gz
#decompress
gzip -d Drosophila_melanogaster.BDGP6.22.dna.toplevel.fa.gz
#rename to something more manageable
mv Drosophila_melanogaster.BDGP6.22.dna.toplevel.fa DmBDGP6.fasta
```

dict: A dictionary file as generated by Picard’s "CreateSequenceDictionary". Needed for Picard Tools.

```
srun java -jar $PICARD CreateSequenceDictionary R=DmBDGP6.fasta O=DmBDGP6.dict
```

gtf: The principle file to determine the location of genomic features like genes, transcripts, and exons. Many other metadata files we use derive from this original file. We download our GTF files from ensembl, which has a handy description of the file format ​here​. Ensembl has a huge number of prepared GTF files for a variety of organisms ​here​.

```
#download
wget ftp://ftp.ensembl.org/pub/release-96/gtf/drosophila_melanogaster/Drosophila_melanogaster.BDGP6.22.96.gtf.gz
#decompress
gzip -d Drosophila_melanogaster.BDGP6.22.96.gtf.gz
#rename to something more manageable
mv Drosophila_melanogaster.BDGP6.22.96.gtf DmBDGP6.gtf
```

refFlat: This file contains a subset of the the same information in the GTF file in a different format. Picard tools like the refFlat format, so we require this as well. To make life easy, we provide a program ConvertToRefFlat that can convert files from GTF format to refFlat for you.

```
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/ConvertToRefFlat ANNOTATIONS_FILE=DmBDGP6.gtf SEQUENCE_DICTIONARY=DmBDGP6.dict OUTPUT=DmBDGP6.refFlat
```
NB: I got an error when using the drosophila gtf file:
*Exception in thread "main" picard.annotation.AnnotationException: Invalid GTF line:
3R FlyBase transcript 722370 722621 . - . gene_id "FBgn0085804"; transcript_id "FBtr0114258"; gene_name "CR41571"; gene_source "FlyBase"; gene_biotype "pseudogene"; transcript_source "FlyBase"; transcript_biotype "pseudogene";
Problems:
Missing transcript_name*

This seems to be due to all sorts of noncoding RNAs, which I simply removed:

```
cat DmBDGP6.gtf |  grep -v 'transcript_biotype "pseudogene"' | grep -v 'transcript_biotype "ncRNA"'  | grep -v 'transcript_biotype "tRNA"' | grep -v 'transcript_biotype "snoRNA"' |      grep -v 'transcript_biotype "pre_miRNA"' | grep -v 'transcript_biotype "snRNA"'   | grep -v 'transcript_biotype "rRNA"' > DmBDGP6.2.gtf
```

Now gives me warnings, but runs:
*INFO 2019-04-27 17:33:33 GTFReader No transcript in GTF for gene tRNA:Leu-CAA-1-1 -- skipping*

I did not produce the following optional files:
* genes.intervals: The genes from the GTF file in ​interval list format​. This file is optional, and useful if you want to go back to your BAM later to see what gene(s) a read aligns to.
* exons.intervals: The exons from the GTF file in ​interval list format​. This file is optional, and useful if you want to go back to your BAM and view what exon(s) a read aligns to.
* rRNA.intervals: The locations of ribosomal RNA in ​interval list format​. This file is optional, but we find it useful to later assess how much of a dropseq library aligns to rRNA.
* reduced.gtf: This file contains a subset of the information in the GTF file, but in a far more human readable format. This file is optional, but can be generated easily by the supplied ReduceGTF program that will take a GTF file as input.


### Make alignment

Once you have an unmapped, queryname-sorted BAM, you can follow this set of steps to align your raw reads and create a BAM file that is suitable to produce digital gene expression (DGE) results.

* Unmapped BAM -> aligned and tagged BAM

```
#Tag cell barcodes
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/TagBamWithReadSequenceExtended INPUT=SRR7276817.bam OUTPUT=SRR7276817_Cell.bam SUMMARY=SRR7276817_Cellular.bam_summary.txt BASE_RANGE=1-12 BASE_QUALITY=10 BARCODED_READ=1 DISCARD_READ=False TAG_NAME=XC NUM_BASES_BELOW_QUALITY=1
#Tag molecular barcodes
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/TagBamWithReadSequenceExtended INPUT=SRR7276817_Cell.bam OUTPUT=SRR7276817_CellMolecular.bam SUMMARY=SRR7276817_Molecular.bam_summary.txt BASE_RANGE=13-20 BASE_QUALITY=10 BARCODED_READ=1 DISCARD_READ=True TAG_NAME=XM NUM_BASES_BELOW_QUALITY=1
```

NB: you can check that the tags have been added properly:

```
module load samtools
#original should have no tags
samtools view SRR7276817.bam | head
#final should have tags
samtools view SRR7276817_CellMolecular.bam | head
```

Remove reads where the cell or molecular barcode has low quality bases

```
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/FilterBam TAG_REJECT=XQ INPUT=SRR7276817_CellMolecular.bam OUTPUT=SRR7276817_tagged_filtered.bam
#Trim 5’ primer sequence
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/TrimStartingSequence INPUT=SRR7276817_tagged_filtered.bam OUTPUT=SRR7276817_tagged_trimmed_smart.bam OUTPUT_SUMMARY=adapter_trimming_report.txt SEQUENCE=AAGCAGTGGTATCAACGCAGAGTGAATGGG MISMATCHES=0 NUM_BASES=5
#Trim 3’ polyA sequence
srun  /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/PolyATrimmer INPUT=SRR7276817_tagged_trimmed_smart.bam OUTPUT=SRR7276817_mc_tagged_polyA_filtered.bam OUTPUT_SUMMARY=polyA_trimming_report.txt MISMATCHES=0 NUM_BASES=6 USE_NEW_TRIMMER=true
```

* SAM -> Fastq

```
srun java -jar $PICARD SamToFastq I=SRR7276817_mc_tagged_polyA_filtered.bam FASTQ=SRR7276817_mc_tagged_polyA_filtered.fastq
```

* STAR alignment

```
#before indexing, create directory for index files
mkdir GenDir

#run commands on SLURM's srun
module load star/2.6.0c

#index genome
srun STAR --runThreadN 30 --runMode genomeGenerate --genomeDir GenDir --genomeFastaFiles DmBDGP6.fasta --sjdbGTFfile DmBDGP6.2.gtf --sjdbOverhang 69

#map reads
srun STAR --runThreadN 30 --genomeDir GenDir --readFilesIn SRR7276817_mc_tagged_polyA_filtered.fastq --outFileNamePrefix star

#Sort STAR alignment in queryname order
srun java -jar $PICARD SortSam I=starAligned.out.sam O=aligned.sorted.bam SO=queryname

#Merge STAR alignment tagged SAM to recover cell/molecular barcodes
srun java -jar $PICARD MergeBamAlignment REFERENCE_SEQUENCE=DmBDGP6.fasta UNMAPPED_BAM=SRR7276817_mc_tagged_polyA_filtered.bam ALIGNED_BAM=aligned.sorted.bam OUTPUT=merged.bam INCLUDE_SECONDARY_ALIGNMENTS=false PAIRED_RUN=false

#Add gene/exon and other annotation tags
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/TagReadWithGeneFunction I=merged.bam O=star_gene_exon_tagged.bam ANNOTATIONS_FILE=DmBDGP6.refFlat
```

* Barcode Repair

```
#Repair substitution errors (DetectBeadSubstitutionErrors)
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/DetectBeadSubstitutionErrors I=star_gene_exon_tagged.bam O=star_clean_subtitution.bam OUTPUT_REPORT=star_clean.substitution_report.txt

#Repair indel errors (DetectBeadSynthesisErrors)
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/DetectBeadSynthesisErrors I=star_clean_subtitution.bam O=star_clean.bam REPORT=my_clean.indel_report.txt OUTPUT_STATS=my.synthesis_stats.txt SUMMARY=my.synthesis_stats.summary.txt PRIMER_SEQUENCE=AAGCAGTGGTATCAACGCAGAGTAC
```

## Part 2: Drop-seq: Digital Gene Expression extraction

In the first part of the protocol, we processed and aligned drop-seq reads. After many several filtering procedures, we obtained the alignment file star_clean.bam, which we need to extract gene expression values/cell from. 

From the drop-seq cookbook: *"There are two outputs available: the primary is the DGE matrix, with each a row for each gene, and a column for each cell. The secondary analysis is a summary of the DGE matrix on a per-cell level, indicating the number of genes and transcripts observed."*



1. OPTION 1: Extract from set number of cells

You can simply tell Drop-Seq to extract expression for the set number of cells that you sequenced. The problem is that it's not easy to know what this number is a priori (and even harder with published data!). So we first use the tool BAMTagHistogram to estimate the number of cells that we have (e.g. the number that seems to capture most of the mapped reads), and then extract gene expression values based on this number. 



1.1 Use BAMTagHistogram to get sense of how many cells we should be extracting from

```
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/BamTagHistogram I=star_clean.bam O=cell_readcounts.txt.gz TAG=XC
```


1.2 Use R to plot the distributtion

```
module load R

R
```

In the R console

```
#we want to output to a pdf file called distribution.pdf
pdf("distribution.pdf", width=10, height=10)

#upload the read counts per cell identifier
a=read.table("cell_readcounts.txt.gz", header=F, stringsAsFactors=F)

#calculate the cumulative sum
x=cumsum(a$V1)
x=x/max(x)

#plot the cumulative distribution
plot(1:length(x), x, type='l', col="blue", xlab="cell barcodes sorted by number of reads [descending]", ylab="cumulative fraction of reads", xlim=c(0,20000))

#our PDF is ready - you have to tell R or you won't be able to open it!
dev.off()

#quit R
q()
```

Here is what mine looks like:

![dropseq](/images/distribution.jpg)



1.2 Extract the expression for the number of cells that you have in your sample

In my case, I will go for 4000, since that seems to contain most of the cells with many reads. 

```
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/DigitalExpression I=star_clean.bam O=DGE_4000cells.txt.gz SUMMARY=DGE_4000cells.summary.txt NUM_CORE_BARCODES=4000
```


2. OPTION 2: Extract based on the number of genes that are expressed per cell

A quality filter that is common when analysing single-cell data is the number of genes expressed per cell - cell with too few genes expressed cannot be analysed properly and should be excluded. While it is possible to implement this filter at a later stage (when we use Seurat to cluster the cells), another option is to only extract expression for cells that have expression for a minimum number of genes. Again, it can be useful to first get a sense of how many genes are expressed per cell in your sample, before you decide on the cut-off. 



2.1 Use previous output file to look at the distribution of genes expressed / cell

If you have no idea of what the number of genes that one expects is (like me), it can be useful to look at the distribution of genes expressed/cell that we got from the 4000 cells with the most reads (this is provided in the previous output file DGE_4000cells.summary.txt). 


In your R console

```
#we want to output to a pdf file called distribution.pdf
pdf("genespercell_distribution.pdf", width=10, height=10)

#upload the summary from the previous DGE extraction
a=read.table("DGE_4000cells.summary.txt", header=T, stringsAsFactors=F)

#plot the distribution
plot(density(a$NUM_GENES), xlab="Number of genes expressed", ylab="Frequency", log="x")

#our PDF is ready - you have to tell R or you won't be able to open it!
dev.off()

#quit R
q()
```

Here is what my plot looks like:

![dropseq](/images/genespercell_distribution.jpg)


The median seems to be about 200 genes expressed per cell, but I will keep all cell that have at least 100 genes.  (NB: I was surprised by the small number of genes, but from the associated paper: "In Seurat analysis we performed initial quality control analysis and low-quality cells were filtered out using 200 and 3000 gene/cell as a low and top cutoff"). 

2.2 Extract gene expression after filtering for minimum number of genes expressed

```
srun /nfs/scistore03/vicosgrp/Bioinformatics_2019/Software/Drop-seq_tools-2.3.0/DigitalExpression I=star_clean.bam O=DGE_100genes.txt.gz SUMMARY=DGE_100genes.summary.txt MIN_NUM_GENES_PER_CELL=100
```


