# BioinformaticsII

Students should develop their own bioinformatics analysis. To get you started, here is a collection of bioinformatics pipelines tailored to our IST cluster. You can combine them to produce your own analysis!

## Topics covered

Here is a list of the topics covered:
* Epigenetics: analysis of ChIP-seq and bisulfite sequencing
* Differential Expression Analysis with Kallisto/Sleuth, Star/DEseq2, and Star/HTseq/EdgeR
* Downloading and cleaning up reads: FastQC, trimming with Trimmomatic and with trimgalore
* Genome / Transcriptome Assembly: genome assembly with SOAPdenovo and with Megahit/SOAPfusion, transcriptome assembly with SOAPdenovo-trans and with Trinity [Evigene pipeline to be added soon!]
* Population Genomics: genetic variant (SNP) calling, PiN/PiS and dN/dS estimation, ancestry analysis, phylogenomics, Fst
* Single-cell sequencing: Drop-seq data analysis, Drop-Seq/10x single cell analysis with Salmon/Alevin


## Get help

We are available:
* on the [IST chat](https://chat.ist.ac.at/channel/bioinformatics2_2020)
* by email (bvicoso@ist.ac.at).

## Examples of project ideas
* reproduce results of a paper (if you don't believe them, or want to learn their methods)
* Use data for different purpose (e.g. RNA-seq data that was used to estimate expression can be used to look at genetic relationship between different populations)
* Combine different datasets into a single analysis (e.g. as we did with ChIP-seq for histone mark and RNA-seq for larval expression)

## Acknowledgments
Pipelines provided by:
* Beatriz Vicoso
* Reka Kelemen
* Christelle Fraisse