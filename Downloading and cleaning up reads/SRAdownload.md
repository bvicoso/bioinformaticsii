# Download DNA or RNA reads from SRA (https://www.ncbi.nlm.nih.gov/sra)

You need to know the SRA run IDs of the reads that you want to download. We will use the following dataset as an example:
 
```
human hapmap individual NA10851 genomic paired-end library

https://www.ncbi.nlm.nih.gov/sra/SRX018839[accn]

SRA first run ID: SRR039537

SRA second run ID: SRR039538
```


In order to download the reads, you need to use the SRA-toolkit (version 2.8.1-3). This is the official program provided by NCBI (ncbi.nlm.nih.gov), where the read are hosted, to download data from their website. 

Using pico, make a new SLURM script (for instance, "pico download.sh"). In the module load / run commands section of your script, add:
```

#load any module you need here

module load SRA-Toolkit/2.8.1-3

 #run commands on SLURM's srun

srun fastq-dump --skip-technical --readids --split-files SRR039537
srun fastq-dump --skip-technical --readids --split-files SRR039538

```

 (Change "SRR039537" to your actual SRA run ID)

 If you have more than one library, you should have the "srun fastq-dump" command twice in the SLURM script, once for each SRA run ID.

Once your script is ready, use "sbatch" to queue it.

 

A short explanation of what each part of the command means:

 fastq-dump : this is the name of the fastq-retrieving program of the SRA toolkit

 --skip-technical : keep only biological reads.

 --readids : keep original read IDs

 --split-files: we are using paired-end reads: for each DNA fragment, both the beginning (forward read) and the end (reverse read) were sequenced. Most programs that we use require two files, sra_1.fast1 (which contains the forward reads) and sra_2.fastq (reverse reads). This option creates these two files.

A more detailed explanation of the options can be found here: https://edwards.sdsu.edu/research/fastq-dump/ 

While this is running, watch the FastQC tutorial: https://www.youtube.com/watch?v=bz93ReOv87Y
