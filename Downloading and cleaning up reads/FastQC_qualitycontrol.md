# Checking the quality of RNA or DNA reads using FastQC

FastQC is a diagnostic tool that will tell you several measures of the quality of your reads. You can run it by putting into a SLURM script:
```
#load modules here
module load java
module load fastqc

#run commands here
srun fastqc SRAID_1.fastq
srun fastqc SRAID_2.fastq
```

 This will create a .html file for each reads file (if you have two libraries, you should have more fastqc commands in the script, and four .html files at the end). 

Let's transfer these files to your laptop so that you can have a look at them. 

**Unix/Mac users**, use "scp": Open a new terminal window and copy the .html files directly to your computer:

```
scp YOUR_LOGIN_NAME@bea81:/nfs/scistore03/vicosgrp/Bioinformatics_2020/YOURGROUPNAME/*html ~/Documents/
```

You should now be able to open the files from the "Documents" folder on your machine (the path to Documents may be different in different operating systems)

**Windows users:** You should be able to browse your cluster files on the right side of the MobaXterm window, and simply drag the html files to your own computer. How do the reads look?

