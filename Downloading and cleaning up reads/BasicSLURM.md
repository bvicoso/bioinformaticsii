# Getting around the Unix command line & job queue on the cluster 

Most of the work that we are going to do will be done on the cluster, which we access remotely. In order to do so: 

Open a terminal window and type: 

```
ssh YOUR_IST_LOGIN@bea81
```


You will be asked: "Are you sure you want to continue connecting (yes/no)?"
Type "yes", then press "enter". It should then prompt you for your password (the same as the one you use for your email).

Or, if you are NOT at IST:

```
ssh -J YOUR_IST_LOGIN@login.ist.ac.at YOUR_IST_LOGIN@bea81
```

(in this case you have to give your password twice)

Make a working directory (change "YOURGROUPNAME" to whatever you would like to be called):

```
mkdir path/to/YOURGROUPNAME_module1
```

And go to the working directory:

```
cd path/to/YOURGROUPNAME_module1
```
 

Run you first command through the cluster queue:

Let's take the simple command: 

```
echo "I am running through the queue"
```

If you type it in the terminal, it will run and print the message. But in order to have access to the fast CPUs of the cluster, you need to put jobs on a queue; once CPUs become available, your job starts.  This requires writing a script (a text file) that tells the cluster 1. which queue to use 2. how much memory you will need 3. what the job that you want to run is. SLURM is the program that manages jobs in the cluster. 


Create and edit a new SLURM script using the text editor Pico: 
```
pico test.sh
```
This opens a new text file, which you can now write into. If you are used to a different text editor, feel free to use it. 

[Tip: I usually prefer to edit my scripts on my desktop using any text editor, and then just copy the edited text into pico at the end.]

Add the following text to your file (the first part should be present in every SLURM script, but with different memory/CPU requirements depending on the job; the second part is your actual list of commands, in this case a single echo command): 

```
#!/bin/bash
#
#we first start by giving the cluster a few options on the job: which queue to put it into (nick), and the name we are giving it

#SBATCH --job-name=test
#SBATCH --output=test_output
#
#Number of CPU cores to use within one node
#SBATCH -c 5
#
#Define the number of hours the job should run.
#Maximum runtime is limited to 10 days, ie. 240 hours
#SBATCH --time=96:00:00
#
#Define the amount of RAM used by your job in GigaBytes
#In shared memory applications this is shared among multiple CPUs
#SBATCH --mem=10G
#
#Send emails when a job starts, it is finished or it exits
#SBATCH --mail-user=youremail@ist.ac.at
#SBATCH --mail-type=ALL
#
#Pick whether you prefer requeue or not. If you use the --requeue
#option, the requeued job script will start from the beginning,
#potentially overwriting your previous progress, so be careful.
#For some people the --requeue option might be desired if their
#application will continue from the last state.

#Do not requeue the job in the case it fails.
#SBATCH --no-requeue
#
#Do not export the local environment to the compute nodes
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV
#
#Set the number of threads to the SLURM internal variable
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#
#load any module you need here
#run commands on SLURM's srun
srun echo "I am running through the queue"
```

To exit pico, type *control+X*. 

Pico will ask you "Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES)?"

type "Y" then "enter", then "enter" again to keep the same name. 

Finally, add the job to the queue by typing: sbatch test.sh

**Note that rather than just writing echo "I am running through the queue", as you did before, you now need to start the line with srun - this is what tells SLURM to queue the job.**

 

You can check the status of your queue by typing: *squeue -u your_user_name*

If you need to cancel a job:
* Copy the JOB_ID of your job (the first column from the squeue output).
* Cancel the job with the command: scancel JOB_ID


Once the job is done running (it no longer appears when you check squeue), you can check the output file test_output by typing:

```
more test_output
```
 
This prints the contents of the file test_output to the screen, one page at a time.

If you see "I am running through the queue", you have successfully used SLURM, and are now ready to work with reads! But if you would like to learn more about the cluster, go to: 

https://it.pages.ist.ac.at/docs/hpc-cluster/

https://wiki.ist.ac.at/index.php/SLURM_Commands

