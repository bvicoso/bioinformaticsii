# Clean reads using Trimmomatic

The program Trimmomatic can be used to trim/remove low quality reads, or reads that have adapter sequences in them. Run it on each pair of fastq files (in a SLURM file; replace SRR039537 with your SRA run ID):

```
#load modules here
module load java
#run commands here
srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -phred33 SRR039537_1.fastq SRR039537_2.fastq  SRR039537_1_paired.fastq SRR039537_1_unpaired.fastq SRR039537_2_paired.fastq SRR039537_2_unpaired.fastq ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
```
 

A short explanation of what each part of the command means:

* java -jar : this is a program written in java; you need to have java installed to use it.

* trimmomatic-0.36.jar : this is the actual java program

* ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 : Remove adaptor sequences used by Illumina.

* LEADING:3 : Remove bad quality base-pairs at the beginning of the read

* TRAILING:3 :  Remove low quality base-pairs at the end of the read.

* SLIDINGWINDOW:4:15 : Run a sliding window through the read and remove sequences of low-quality base-pairs in the middle of the read.

* MINLEN:36 : Dump all reads that are smaller than 36 base-pairs after the trimming procedure.

A detailed explanation can be found here: http://www.usadellab.org/cms/?page=trimmomatic

