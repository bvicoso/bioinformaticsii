# Pipeline to quickly infer strand specificity of RNA-seq libraries

Step 1: run Trinity assembly with option  --SS_lib_type RF (paired end) / --SS_lib_type F (single end)
Step 2: Check if transcripts have a longer orf in forward (RF correct) or reverse (FR correct) strand, or in neither strand (unstranded RNA-seq)
Step 2 (single-end): Check if transcripts have a longer orf in forward (F correct) or reverse (R correct) strand, or in neither strand (unstranded RNA-seq)

## Step 1: run trinity assembly

```
#paired end:
Trinity --seqType fq --left $run_1.fastq --right $run_2.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
#single end:
Trinity --seqType fq --single $run_1.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
```


## Step 2: run ORFtoStrand.pl 

```

```