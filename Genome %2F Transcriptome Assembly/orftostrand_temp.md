
```
module load fafilter/v.357
faFilter -minSize=1000 trinity_genes.fasta trinity_genes_1000.fasta 
perl orftostrand1.pl trinity_genes_1000.fasta 

#Get distribution of length for forward frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds

#Get distibution of length for reverse frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds2

# compare them using R
module load R
~/scripts/ORF_to_strand$ Rscript orftostrand2.Rscript 
```