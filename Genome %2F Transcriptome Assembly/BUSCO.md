# BUSCO quality control

Check how good your genome or transcriptome assemblies are using [BUSCO](http://busco.ezlab.org/).

BUSCO uses a set of genes that are conserved in all eukaryotes to check how good your genome or transcriptome assembly is: in a perfect assembly, all of these conserved genes should be found, and they should each be on a single scaffold! If your assembly is terrible, many will be missing or fragmented.

First, let's use the command "wget" to download the set of conserved genes for all eukaryotes from the BUSCO website:

```
wget http://busco.ezlab.org/v2/datasets/eukaryota_odb9.tar.gz
```

["wget" allows you to download the contents of a URL directly to the cluster. You can pick which dataset to download by going to the website http://busco.ezlab.org/ and clicking on "Download all datasets" - links for all the available sets are provided.]

Use ls to check that you now have the file eukaryota_odb9.tar.gz in your directory, then use the following command to decompress it:

```
tar -zxvf eukaryota_odb9.tar.gz
```


## BUSCO quality check for genome

In a SLURM script:

```
#load any modules here
module load busco
module load ncbi-blast
module load hmmer
module load augustus
#run commands here
srun run_BUSCO.py -f -i XXX.scafSeq -o busco_GROUPNAME -l eukaryota_odb9  -m geno -c 16 -sp human/fly
```

 (pick between human and fly)

 

## BUSCO quality check for transcriptome

In a SLURM script:

```
#load any module here
module load busco
module load ncbi-blast
module load hmmer

#run commands here
srun run_BUSCO.py -f -i XXXX.scafSeq -o busco_GROUPNAME -l eukaryota_odb9 -m tran -c 16 -sp human/fly
```

(pick between human and fly)

