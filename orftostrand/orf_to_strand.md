# Pipeline to quickly infer strand specificity of RNA-seq libraries

Step 1: run Trinity assembly with option  --SS_lib_type RF (paired end) / --SS_lib_type F (single end)

Step 2: Check if transcripts have a longer orf in forward (RF correct) or reverse (FR correct) strand, or in neither strand (unstranded RNA-seq)

Step 2 (single-end): Check if transcripts have a longer orf in forward (F correct) or reverse (R correct) strand, or in neither strand (unstranded RNA-seq)

## Step 1: run trinity assembly

```
#paired end:
Trinity --seqType fq --left $run_1.fastq --right $run_2.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
#single end:
Trinity --seqType fq --single $run_1.fastq --output $run_Trinity --CPU 50 --max_memory 20G --trimmomatic --SS_lib_type RF
```


## Step 2: run ORFtoStrand pipeline 

```
# Keep only long transcripts (make sure to use the output name trinity_genes_1000.fasta or change the final R script)
module load fafilter/v.357
faFilter -minSize=1000 trinity_genes.fasta trinity_genes_1000.fasta 
perl orftostrand1.pl trinity_genes_1000.fasta 

#Get distribution of length for forward frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds

#Get distibution of length for reverse frames
perl getlength_v2.pl trinity_genes_1000.fasta.cds2

# compare them using R
module load R
~/scripts/ORF_to_strand$ Rscript orftostrand2.Rscript 

```


## Alternative: skip assembly altogether

```

# Download reads
fastq-dump -X 1000000 --split-files --skip-technical SRArun
tail -10000 SRArun_1.fq > forward.fasta
tail -10000 SRArun_2.fq > reverse.fasta

#OR

zcat Plodia_Adult_Heads_F3_1.fastq.gz | grep -A 1 '^@' | perl -pi -e 's/^@/>/gi' | perl -pi -e 's/^--.*\n//gi' | head -1000000 | tail -10000 > forward.fasta
zcat Plodia_Adult_Heads_F3_2.fastq.gz | grep -A 1 '^@' | perl -pi -e 's/^@/>/gi' | perl -pi -e 's/^--.*\n//gi' | head -1000000 | tail -10000 > reverse.fasta

perl ../orftostrand1.pl forward.fasta
perl ../orftostrand1.pl reverse.fasta

perl ../getlength_v2.pl forward.fasta.cds
perl ../getlength_v2.pl forward.fasta.cds2

perl ../getlength_v2.pl reverse.fasta.cds
perl ../getlength_v2.pl reverse.fasta.cds2

perl -pi -e 's/\#.* / /gi' *length

module load R
R

forward<-read.table("forward.fasta.cds.length", sep=" ", head=F)
 rev<-read.table("forward.fasta.cds2.length", sep=" ", head=F)
 median_forward<-median(forward[,2])
 median_rev<-median(rev[,2])
 pval<-wilcox.test(forward[,2], rev[,2], paired=T)$p.value
forrev<-cbind (forward, rev)
forw_big<-dim(subset(forrev, forrev[,2]>forrev[,4]))[1]
rev_big<-dim(subset(forrev, forrev[,2]<forrev[,4]))[1]
 results<-paste ("Forward READ:", "Median forward ORF:", median_forward, "Median reverse ORF:", median_rev, "p-value", pval, "// Number of transcripts with F>R:", forw_big, "// Number of transcripts with R>F:", rev_big, sep=" ")
results
 write(results, file="ORF_to_Strand_results.txt", append=T)

forward<-read.table("reverse.fasta.cds.length", sep=" ", head=F)
 rev<-read.table("reverse.fasta.cds2.length", sep=" ", head=F)
 median_forward<-median(forward[,2])
 median_rev<-median(rev[,2])
 pval<-wilcox.test(forward[,2], rev[,2], paired=T)$p.value
forrev<-cbind (forward, rev)
forw_big<-dim(subset(forrev, forrev[,2]>forrev[,4]))[1]
rev_big<-dim(subset(forrev, forrev[,2]<forrev[,4]))[1]
 results<-paste ("Reverse READ:", "Median forward ORF:", median_forward, "Median reverse ORF:", median_rev, "p-value", pval, "// Number of transcripts with F>R:", forw_big, "// Number of transcripts with R>F:", rev_big, sep=" ")
results
 write(results, file="ORF_to_Strand_results.txt", append=T)
```