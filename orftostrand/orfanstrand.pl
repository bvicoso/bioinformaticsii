#!/usr/bin/perl

#usage: perl orfanstrand.pl SRA_runs.txt

#modules to load
#module load R

my $list = $ARGV[0];

open (INPUT, "$list") or die "can't find $input";

while ($run = <INPUT>) 
	{
	chomp $run;
	print "$run\n";

	#####################3.1 Download fastq files
	#first check if the reads already exist
	$filename = "$run\_1.fastq";

	system "echo $run >> ORF_to_Strand_results.txt";

	if (-e $filename)
		{
		print "$run_1.fastq already exists\n";
		}	
	else
		{
		#download only the first 1 million reads of each file so that we don't wait forever when there are huge ones
		system "fastq-dump -X 1000000 --split-files --skip-technical $run";


		#####################3.2 Run Bowtie2

		#check if XXX_2 exists
		$filename2="$run\_2.fastq";
		if (-e $filename2)
			{

			#if yes
			system "cat $run_1.fastq  | grep -A 1 \'^@\' | perl -pi -e \'s/^@/>/gi\' | perl -pi -e \'s/^--.*\n//gi\' | head -1000000 | tail -10000 > forward.fasta";
		    system "cat $run_2.fastq  | grep -A 1 \'^@\' | perl -pi -e \'s/^@/>/gi\' | perl -pi -e \'s/^--.*\n//gi\' | head -1000000 | tail -10000 > reverse.fasta";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/orftostrand1.pl forward.fasta";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/orftostrand1.pl reverse.fasta";

            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl forward.fasta.cds";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl forward.fasta.cds2";

            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl reverse.fasta.cds";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl reverse.fasta.cds2";

            system "perl -pi -e \'s/\#.* / /gi\' *length";
			system "Rscript /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/orftostrand2b_PEreads.Rscript";
			}
		else
			{

			#if no
			print "$run is single-end\n";
			system "cat $run_1.fastq  | grep -A 1 \'^@\' | perl -pi -e \'s/^@/>/gi\' | perl -pi -e \'s/^--.*\n//gi\' | head -1000000 | tail -10000 > forward.fasta";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/orftostrand1.pl forward.fasta";

            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl forward.fasta.cds";
            system "perl /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/getlength_v2.pl forward.fasta.cds2";

            system "perl -pi -e \'s/\#.* / /gi\' *length";			
            system "Rscript /nfs/scistore18/vicosgrp/bvicoso/scripts/ORF_to_strand/orftostrand2c_SEreads.Rscript";

			}

		#####################3.3 remove file
		system "rm $run\_1.fastq $run\_2.fastq";




		}

	}
