# ChIP-seq: peak calling with bowtie2, Picard and MACS2

We are using the following Drosophila melanogaster dataset: 
H3K4me2 ChIP-seq: https://trace.ncbi.nlm.nih.gov/Traces/sra/?study=SRP023385 
Consisting of two single-end libraries of control ("input") and two libraries for the H3K4me2 ChIP-seq.

## Alignment of reads to the genome

Let's use the download the D. melanogaster genome from www.ensembl.org (same as getting the transcriptome, but this time select "DNA" instead of "cDNA").
We want to download all the files that contain chromosomes, such as: Drosophila_melanogaster.BDGP6.28.dna.chromosome.2L.fa.gz
We can use wget to do this!

```
wget ftp://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/dna/Drosophila_melanogaster.BDGP6.28.dna.chromosome.*.fa.gz 
```

We need to decompress the chromosome gzipped files so that bowtie2 can use them, and concatenate them into a single file.
We can do this using zcat, which directly reads from gzipped files and outputs them to the screen (in this case, we're instead sending the output to the file DrosophilaChromosomes.fa):

```
zcat Drosophila_melanogaster.BDGP6.28.dna.chromosome.*.fa.gz > DrosophilaChromosomes.fa
```

The first step is to index the genome:

```
#load any modules here
module load bowtie2/2.2.9
#run commands here
srun bowtie2-build DrosophilaChromosomes.fa DrosoGenome
```

Once the indexing is finishe, start the alignment.

```
#load any modules here
module load bowtie2/2.2.9
#run commands here
srun bowtie2 -x DrosoGenome -U XXX_trimmed.fq -p 8 -S XXX.sam
```

(you should have two control replicates and two ChIP-seq replicates, so there should be four bowtie2 commands in the SLURM script)

 

## Keep only uniquely mapped reads reads

Let's have a look at what SAM files look like. Type:
head SRR869918.sam
Then: tail SRR869918.sam

These two commands show you the beginning and end of the file. The file starts with a list of all the chromosomes/scaffolds in your genome (lines starting with "@").
After this list, you have the actual SAM alignment: one read per line, starting with the read name, followed by information on where and how the read aligns in the genome (see our first lecture for details).

Some reads will have the expression "XS:i:" in their alignment - this means that they map to more than a single place in the genome, which could lead to problems when calling peaks. We remove by telling grep to select only lines that DO NOT (-v) match "XS:i:". Don't forget to do this using a SLURM script, as these are large files!

```
srun grep -v 'XS:i:' XXX.sam > XXX_uniq.sam
```

## Removal of PCR duplicates 

Another common problem that can lead to false positives in the presence of read duplicates that result from over-amplification during the library preparation. 
Picard is a program that can manipulate SAM and BAM files. Here, we will first create a BAM file that has the aligned reads sorted by coordinate on the chromosomes. Picard will then use this sorted BAM alignment to remove PCR duplicates, and store the remaining reads in a new BAM file. 

In a SLURM script, run the following commands for the first sample (SRR869918); make sure to ask for  AT LEAST 20GB OF RAM:

```
#load any modules here
module load picard/2.13.2
#run commands here
srun java -jar $PICARD SortSam I=SRR869918_uniq.sam O=SRR869918_sorted.bam SORT_ORDER=coordinate
srun java -jar $PICARD MarkDuplicates  I=SRR869918_sorted.bam  O=SRR869918_nodups.bam  M=SRR869918_marked_dup_metrics.txt REMOVE_DUPLICATES=true ASSUME_SORTED=false
```

At the end of the output file of your SLURM script, you will find:
* the total number of uniquely mapped reads ("Read XX records.")
* the number of uniquely mapped reads that were flagged as duplicates ("Marking XX records as duplicates.")
You can use less to find and record these numbers, and calculate for each library:
Non-Redundant Fraction (NRF) = Number of distinct uniquely mapping reads (i.e. after removing duplicates) / Total number of reads.

Check that they are up to the Encode standards:
https://www.encodeproject.org/data-standards/terms/#library 
  

## Merge two BAM files into one 

We have two replicates per sample, and can therefore choose one of two approaches:
*  call peaks in each separately, and then find a set of consensus peaks (peaks that are present in both)
* merge the BAM files for the two controls and the two replicates and get a single set of peaks.
We use the second option.

Picard can be used to merge the two files through the following commands:

```
#load any modules here
module load picard/2.13.2
#run commands here
srun java -jar $PICARD MergeSamFiles I=SRR869918_nodups.bam I=SRR869919_nodups.bam O=H3K4me2_input.bam
srun java -jar $PICARD MergeSamFiles I=SRR869920_nodups.bam I=SRR869921_nodups.bam O=H3K4me2_chipseq.bam
```
 

## Call peaks

Let's use the program MACS2 to call peaks from the input and the ChIP-seq BAM files. The options for this are:
-t treatment (ChIP-seq) BAM file
-c control (input) BAM file
-f format
-g genome size (dm = Drosophila melanogaster)
-n  name to give the output files
-q false discovery rate

Run the following command in using a SLURM script: 

```
#load any modules here
module load macs2/2.1.0 
#run commands here
srun macs2 callpeak -t H3K4me2_chipseq.bam -c H3K4me2_input.bam -f BAM -g dm -n H3K4me2_chip -q 0.01 
```

Once MACS2 is done running, you should have the following files:

 * H3K4me2_chip_model.r  : You can run this R command file to obtain a summary figure of how peaks were estimated from the positive and negative strand reads, by typing:

```
module load R
Rscript H3K4me2_chip_model.r
```

This will produce a pdf modeling the peaks obtained from each strand. Mine looks like this:
![ChipSeq](/images/H3K4me2_chip_model.jpg)

As expected, we get two peaks that are similar in shape but shifted in their genomic location, suggesting that the peaks we are calling reflect true biological signals. MACS2 uses the distance between these two strand-specific peaks to estimate the fragment size of your libraries, an important parameter when calling peaks.

* H3K4me2_chip_peaks.narrowPeak : A list of peaks, containing the following columns: chromosome, start, end, peak_name, score, strand, fold_enrichment, -log10(p-value), -log10(q-value), relative_summit_position_to_start
* H3K4me2_chip_summits.bed : A list of the peak, their summit, their location, and -log10(p-value).
* H3K4me2_chip_peaks.xls : you can open this file in Excel (but it's just a tab separated file). It has a user-friendly version of each of the peaks.       

 
## Peak annotation

Use either scp or the MobaXterm file browser to copy the file H3K4me2_chip_peaks.narrowPeak to your laptop.
This file contains all the peaks, their location and significance level. We now want to know which functional categories of the genome they are found in.

Let's annotate our peaks by going to:
https://manticore.niehs.nih.gov/pavis2/

Change the "Species/Genome Assembly/Gene Set:" option to "D. melanogaster Flybase R6.05. "
Upload your peaks file to "The query peak file to be annotated:".                                                                
Then submit!

You should see an enrichment table: were your peaks enriched for any kind of functional annotation? Is this what you were expecting? (keep this table for your report) 
Right-click to save the "Full annotation table".


## Remove files 

Since SAM files take up a lot of space, remove them when you are done with your ChIP-seq analysis!
```
rm *.sam
```