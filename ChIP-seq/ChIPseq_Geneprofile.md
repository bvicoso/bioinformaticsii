## Average gene profiles with NGSplot

We are using the following Drosophila melanogaster dataset: 
H3K4me2 ChIP-seq: https://trace.ncbi.nlm.nih.gov/Traces/sra/?study=SRP023385 
Consisting of two single-end libraries of control ("input") and two libraries for the H3K4me2 ChIP-seq.

## Alignment of reads to the genome

Let's use the download the D. melanogaster genome from www.ensembl.org (same as getting the transcriptome, but this time select "DNA" instead of "cDNA").
We want to download all the files that contain chromosomes, such as: Drosophila_melanogaster.BDGP6.28.dna.chromosome.2L.fa.gz
We can use wget to do this!

```
wget ftp://ftp.ensembl.org/pub/release-99/fasta/drosophila_melanogaster/dna/Drosophila_melanogaster.BDGP6.28.dna.chromosome.*.fa.gz 
```

We need to decompress the chromosome gzipped files so that bowtie2 can use them, and concatenate them into a single file.
We can do this using zcat, which directly reads from gzipped files and outputs them to the screen (in this case, we're instead sending the output to the file DrosophilaChromosomes.fa):

```
zcat Drosophila_melanogaster.BDGP6.28.dna.chromosome.*.fa.gz > DrosophilaChromosomes.fa
```

The first step is to index the genome:

```
#load any modules here
module load bowtie2/2.2.9
#run commands here
srun bowtie2-build DrosophilaChromosomes.fa DrosoGenome
```

Once the indexing is finishe, start the alignment.

```
#load any modules here
module load bowtie2/2.2.9
#run commands here
srun bowtie2 -x DrosoGenome -U XXX_trimmed.fq -p 8 -S XXX.sam
```

(you should have two control replicates and two ChIP-seq replicates, so there should be four bowtie2 commands in the SLURM script)


## Keep only uniquely mapped reads reads

Let's have a look at what SAM files look like. Type:
head SRR869918.sam
Then: tail SRR869918.sam

These two commands show you the beginning and end of the file. The file starts with a list of all the chromosomes/scaffolds in your genome (lines starting with "@").
After this list, you have the actual SAM alignment: one read per line, starting with the read name, followed by information on where and how the read aligns in the genome (see our first lecture for details).

Some reads will have the expression "XS:i:" in their alignment - this means that they map to more than a single place in the genome, which could lead to problems when calling peaks. We remove by telling grep to select only lines that DO NOT (-v) match "XS:i:". Don't forget to do this using a SLURM script, as these are large files!

```
srun grep -v 'XS:i:' XXX.sam > XXX_uniq.sam
```

## Removal of PCR duplicates 

Another common problem that can lead to false positives in the presence of read duplicates that result from over-amplification during the library preparation. 
Picard is a program that can manipulate SAM and BAM files. Here, we will first create a BAM file that has the aligned reads sorted by coordinate on the chromosomes. Picard will then use this sorted BAM alignment to remove PCR duplicates, and store the remaining reads in a new BAM file. 

In a SLURM script, run the following commands for the first sample (SRR869918); make sure to ask for  AT LEAST 20GB OF RAM:

```
#load any modules here
module load picard/2.13.2
#run commands here
srun java -jar $PICARD SortSam I=SRR869918_uniq.sam O=SRR869918_sorted.bam SORT_ORDER=coordinate
srun java -jar $PICARD MarkDuplicates  I=SRR869918_sorted.bam  O=SRR869918_nodups.bam  M=SRR869918_marked_dup_metrics.txt REMOVE_DUPLICATES=true ASSUME_SORTED=false
```

At the end of the output file of your SLURM script, you will find:
* the total number of uniquely mapped reads ("Read XX records.")
* the number of uniquely mapped reads that were flagged as duplicates ("Marking XX records as duplicates.")
You can use less to find and record these numbers, and calculate for each library:
Non-Redundant Fraction (NRF) = Number of distinct uniquely mapping reads (i.e. after removing duplicates) / Total number of reads.

Check that they are up to the Encode standards:
https://www.encodeproject.org/data-standards/terms/#library 
  

## Merge two BAM files into one 

We have two replicates per sample, and can therefore choose one of two approaches:
*  call peaks in each separately, and then find a set of consensus peaks (peaks that are present in both)
* merge the BAM files for the two controls and the two replicates and get a single set of peaks.
We use the second option.

Picard can be used to merge the two files through the following commands:

```
#load any modules here
module load picard/2.13.2
#run commands here
srun java -jar $PICARD MergeSamFiles I=SRR869918_nodups.bam I=SRR869919_nodups.bam O=H3K4me2_input.bam
srun java -jar $PICARD MergeSamFiles I=SRR869920_nodups.bam I=SRR869921_nodups.bam O=H3K4me2_chipseq.bam
```
 

# NGSplos

We will use NGSplot to create average H3K4me2 binding gene profiles. It may be useful to have a look at their manual and examples here:

https://github.com/shenlab-sinai/ngsplot

NB: NGSplot requires a formatted version of the genome you are working with in order to function. When you first install it, it only comes with the most recent version of the human genome - IT installed the D. melanogaster one for us. You can get others from:
https://drive.google.com/drive/folders/0B1PVLadG_dCKNEsybkh5TE9XZ1E
(but you need to ask IT to add them)
 

## Index the BAM files

NGSplot requires indexed BAM files, which we produce using SAMtools (a program that can do all sorts of manipulations on SAM/BAM alignments):

```
#load any module here
module load samtools/1.8
#run commands here
srun samtools index H3K4me2_chipseq.bam
srun samtools index H3K4me2_input.bam
```

Check the queue to see if your job is done.  You should have two .bam.bai files when it is!


## Average counts around the TSS in the ChIP and input data

 Let's first get an overview of the average gene profile for the ChIP-seq and input data separately [make sure to ask for at least 30Gb of RAM!]:

```
#load any module here
source /etc/profile.d/modules.sh
module load ngsplot/2.61 
#run commands here
srun ngs.plot.r -G dm6 -R tss -C H3K4me2_input.bam -O Input.tss -T Input -L 3000
srun ngs.plot.r -G dm6 -R tss -C H3K4me2_chipseq.bam -O ChIP.tss -T ChIP-seq -L 3000
```

Some important options:
-G dm6: use the Drosophila melanogaster (version 6) genome as a reference. This has to correspond to the genome version that the ChIP-seq data was mapped to to create the BAM files.
-R tss: The plot will be centered at the transcription start site.
-L 3000: the plot will show 3000bps before and after the TSS.

You should now have several pdfs:
* ChIP.tss.avgprof.pdf - This is the average gene profile plot for your ChIP-seq data.
* ChIP.tss.heatmap.pdf - This shows you the average read count along each gene in the genome, clustered by how similar their profiles are.
* Input.tss.avgprof.pdf, Input.tss.heatmap.pdf - the same for your input data.

Open the two average profile pdfs (XX.avgprof.pdf) using scp: do they look different from each other? Are you convinced that there is a biological signal in your ChIP-seq data?

## Enrichment of H3K4me2 along the gene body

We can use both the ChIP and input data at once: NGSplot will then measure enrichment in the ChIP relative to the input data!
Note that we now use -R genebody argument: this will allow us to look at how H3K4me2 is distributed along the whole gene body (from the TSS to the final stop codon).

```
#load any module here
source /etc/profile.d/modules.sh
module load ngsplot/2.61
#run commands here
srun ngs.plot.r -G dm6 -R genebody -C H3K4me2_chipseq.bam:H3K4me2_input.bam -O H3k4me2vsInp.tss -T H3K4me2 -FL 300
```

From the average profile plot (H3k4me2vsInp.tss.avgprof.pdf), where is H3K4me2 primarily enriched?

## H3K4me2 binding profile at high and low expression genes 

We have lists of high, medium, low and no expression genes in the same folder that we are working in. We will use them to create H3K4me2 binding profiles for the different expression levels, to see if enrichment in H3K4me2 is associated with higher or lower expression levels. 

First, we need to make a configuration file that tells NGSplot where we put our lists of high, medium, low and no expression. Using pico, create the file config.txt and add the following text to it [don't copy the html below, as tabs have been turned into spaces! Instead click on this link and copy from the simple text]: 
```
# If you want to specify the gene list as "genome", use "-1".
# Use TAB to separate the three columns: coverage file<TAB>gene list<TAB>title
# "title" will be shown in the figure's legend.
H3K4me2_chipseq.bam     genes_high.txt  "High"
H3K4me2_chipseq.bam     genes_medium.txt        "Med"
H3K4me2_chipseq.bam     genes_low.txt   "Low"
H3K4me2_chipseq.bam     genes_noexp.txt "No"
```
 
You can then run NGSplot with this configuration file:

```
#load any modules here
source /etc/profile.d/modules.sh
module load ngsplot/2.61
#run commands here
srun ngs.plot.r -G dm6 -R tss -C config.txt -O H3K4me2_highlow -D ensembl -L 3000
```
 